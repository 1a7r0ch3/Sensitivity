# requires tools.r

kQDM_SA <- function(X, Y, kX = rbf_kernel_matrix, kY = NULL, w = NULL, 
    cond = "hybrid", norm = FALSE)
#
#       kQDM_SA(X, Y, kX = rbf_kernel_matrix, kY = kX, w = NULL, cond = 
#           "hybrid", norm = FALSE)
#
# conduct sensitivity analysis by measuring dependence between input factors
# Xi and output Y with kernel quadratic dependence measure (a.k.a.
# Hilbert-Schmidt independence criterion).
#
# INPUTS: 
#   X  - n-by-d data frame, n observations of d input factors, each as n-by-pi
#           matrix, in dimension pi.
#   Y  - n-by-q matrix, n model outputs of dimension q
#   kX - function computing the kernel matrix on X (n-by-pi matrix to n-by-n
#        matrix), or list of d such functions specifying a different kernel for
#        each input factor. provided in file tools.r:
#        rbf_kernel_matrix for classical radial kernels,
#        dCov_kernel_matrix for distance covariance kQDM
#        class_kernel_matrix for categorical contingencies
#        If setting weights on the distribution of the observations changes the
#        resulting kernel matrix, the function should take the weights as argument
#        name "w" (see e.g. rbf_kernel_matrix).
#   kY - function for computing the kernel matrix on Y
#        default to the first kernel given for X
#   w  - weights modifying the distribution of the observations, useful for
#        conditional sensitivity; vector of length n summing to unity
#   cond - type of conditional sensitivity analysis; one of
#          "hybrid": hybrid conditional and target sensitivity analysis,
#              in essence, only the distribution of Y is modified, behaving
#              as a weighted modification of the kernel on Y
#          "conditional": plain conditional sensitivity analysis,
#              both distributions of X and Y are modified
#   norm - tells if normalizations should be computed, see OUTPUTS for details
#
# OUTPUT:
#   kQDM between Y and each input factor Xi are regrouped in vectors of length
#   d. If norm = TRUE, the results are regrouped in a list of three elements:
#       XY - above vector
#       XX - vector of length d, kQDM between each input factor Xi and itself
#       YY - kQDM between Y and itself
#   it is up to the user to normalize the results if deemed useful
#
# Hugo Raguet 2017
{
    d <- length(X)
    n <- nrow(X)
    if (!is.list(kX)){ kX <- rep(list(kX), d) }
    if (is.null(kY)){ kY <- kX[[1]] }
    # in case of hybrid sensitivity analysis, results are more comparable to
    # unconditional sensitivity analysis if weights sum to n
    if (!is.null(w) && cond == "hybrid"){ w <- n*w }
    ##  preprocessing on Y
    # compute kernel matrix
    if (cond == "conditional" && "w" %in% names(formals(kY))){
        KY <- kY(Y, w = w)
    }else{
        KY <- kY(Y)
    }
    if (!is.null(w) && cond == "hybrid"){
        KY <- scal_mul_row_col(KY, w) # as if the kernel on Y was weighted
    }
    if (is.null(w) || cond == "hybrid"){
        KY <- sweep(KY, 2, colMeans(KY), "-") # remove column means
    }else{ # conditional
        KY <- sweep(KY, 2, w%*%KY, "-") # remove column weighted means
    }
    if (norm){
        if (is.null(w) || cond == "hybrid"){ YY <- sum(t(KY)*KY)/(n^2) }
        else{ YY <- drop(w%*%(t(KY)*KY)%*%w) }
    }
    ##  actual kQDM and normalisations
    XX <- XY <- sapply(X, function(Xi){0})
    for (i in 1:d){
        # compute kernel matrix
        if (cond == "conditional" && "w" %in% names(formals(kX[[i]]))){
            KX <- kX[[i]](X[[i]], w = w) 
        }else{
            KX <- kX[[i]](X[[i]])
        }
        if (is.null(w) || cond == "hybrid"){
            KX <- sweep(KX, 1, rowMeans(KX), "-") # remove row means
        }else{ # conditional
            KX <- sweep(KX, 1, KX%*%w, "-") # remove row weighted means
        }
        if (is.null(w) || cond == "hybrid"){ XY[i] <- sum(KX*KY)/(n^2) }
        else{ XY[i] <- w%*%(KX*KY)%*%w } # conditional
        if (norm){
            if (is.null(w) || cond == "hybrid"){
                XX[i] <- sum(KX*t(KX))/(n^2)
            }else{
                XX[i] <- w%*%(KX*t(KX))%*%w
            }
        }
    }
    if (norm){ return(list(XY = XY, XX = XX, YY = YY)) }else{ return(XY) }
}
