# requires tools.r

CDM_SA <- function(X, Y, f = fKL, dest_kernel = Gauss_Silverman_kernel_matrix,
    w = NULL, cond = "hybrid", supp = TRUE, sepa = NULL, norm = FALSE)
#
#   CDM_SA <- function(X, Y, f = fKL, dest_kernel = 
#       Gauss_Silverman_kernel_matrix, w = NULL, cond = "hybrid", supp = TRUE,
#       sepa = NULL, norm = FALSE)
#
# Conduct sensitivity analysis by measuring dependence between input factors
# Xi and output Y, by estimating the Csiszar divergence between the product of
# the marginals P_Xi(x)P_Y and the joint distribution P_(Xi,Y).
#
# We define here the Csiszar divergence between distributions P and Q as
#           div(P,Q;f) = Int f((dP/dM)/(dQ/dM)) dQ,
# where f is a real function supposed to be convex and vanishing at one, and M
# is a measure such that P << M and Q << M, and where the integral is specially
# modified for taking into account the event {dQ/dM = 0}.
#
# Handle situations where Xi and Y are either discrete (i.e. categorical, M
# is a counting measure, *input must be of type `logical`, `character` or 
# `integer`) or continuous (M is the Lebesgue measure, *input must then be of
# type `double` only*).
# For continuous coordinates, probability density functions must be estimated
# by a function specified in `dest_kernel`, returning a "kernel matrix" n-by-n
# such that the density at observation i is estimated by the mean value the
# i-th row. If setting weights on the distribution of the observations changes
# the resulting kernel matrix, the function should take the weights as argument
# name "w" (see e.g. Gauss_Silverman_kernel_matrix).
# Density estimation kernel functions available in file tools.r:
#   Gauss_Silverman_kernel_matrix: Gaussian kernel with diagonal bandwidth
#       following Silverman's rule of thumb (1986) [default]
#   Gauss_adaptive_kernel_matrix: adaptive Gaussian kernel whose pilot is
#       the previous one, and adaptive power 1/2
#   trunc_knn_kernel_matrix: truncated k nearest-neighbors (with Chebyshev
#       distance), k set as n^(4/p+4) for dimension p;
#       WARNING: lower and upper bounds of the variables must be properly set;
#       default to 0 and 1 intended for *copula transforms*, which must be
#       applied beforehand (see function copula_col in tools.r). This method
#       should be largely for bounded variables, or in high dimension.
# It is possible to set the kernel for the couple (X, Y) as the separable
# product of the kernels for X and for Y, by setting sepa = TRUE.
# This gains computational time, but might degrade accuracy. Separability is
# mandatory if `supp` is set to FALSE (see below).
#
# In general, Csiszar divergence are not symmetrical, and there is a choice
# to make between div(P_Xi(x)P_Y, P_(Xi,Y)) and div(P_(Xi,Y), P_Xi(x)P_Y).
#  (i) For the former, we simply exclude from the integral the event
# {dP_(Xi,Y)/dM = 0}, this is the default "support" version (supp = TRUE).
# An important consequence is that Jensen's inequality reads
#           sdiv(P,Q;f) >= f(Int_{supp(Q)} dP/dQ dQ),
# but we only get 0 < Int_{supp(P)} dP/dQ dQ <= Int dP = 1. As a
# result, nonnegativity is ensured only if f is nonnegative over ]0, 1[.
#  (ii) With the latter there is no such subtleties, *but the estimation is less
# robust and costs O(n^3) instead of O(n^2)*; set supp = FALSE to use it, this
# also switches sepa = TRUE.
#
# INPUTS: *categorical variables must be given as factors*
#   X - n-by-d data frame, n observations of d input factors, each as n-by-pi
#       matrix, in dimension pi.
#   Y - n-by-q matrix, n model outputs of dimension q
#   f - discrepency function, or list of discrepency functions
#       provided in this file:
#       fKL (Kullback-Leibler): t -> -log(t)
#       this leads actually to the reverse Kullback-Leibler, KL(Q, P); for our
#       support version (see above) it is however better than the original
#       obtained with the f*: t -> t log(t), which is negative on ]0,1[;
#       moreover in our conventions, the resulting dependence measure is the
#       common definition of the mutual information between Xi and Y
#       fTV (total variation): t -> |t - 1|
#       fHellinger:            t -> (sqrt(t) - 1)^2
#       fPearson:              t -> (t - 1)^2
#       fNeyman:               t -> (t - 1)^2/t
#   dest_kernel - technique for density estimation; function with input n-by-p
#        matrix and output n-by-n density estimation kernel matrix (see above).
#   w - weights modifying the distribution of the observations, useful for
#       conditional sensitivity; vector of length n summing to unity
#   cond - type of conditional sensitivity analysis; one of
#           "hybrid": hybrid conditional and target sensitivity analysis,
#               in essence, only the distribution of Y is modified, resulting
#               in weighting only the integral
#           "conditional": plain conditional sensitivity analysis,
#               both distributions of X and Y are modified
#   supp - if TRUE, the support version of the Csiszar divergence dependance
#           measure is used (see above)
#   sepa - if TRUE, define the density estimation kernel on the product space
#           of (X, Y) as the separable product of the kernels on X and Y.
#   norm - tells if normalizations should be computed, see OUTPUTS for details
#
# OUTPUT:
#   Csiszar divergence dependence measure between Y and each input factor Xi
#   are regrouped in vectors of length d. If norm = TRUE, the results
#   are regrouped in a list of three elements:
#       XY - above vector
#       XX - vector of length d, Csiszar divergence dependence measure between
#            each input factor Xi and itself (which is a naive estimate of 
#            Int f(dP_Xi/dM) dP_Xi)
#       YY - Csiszar divergence dependence measure between Y and itself;
#   it is up to the user to normalize the results if deemed useful;
#   if several functions are given, return a list of above vectors or lists
#
# Hugo Raguet 2017
{
    if (!supp && identical(sepa, FALSE)){ # check if sepa has been set by user
        warning("efficient computation of full Csiszar divergence",
            "requires \"separable\" density estimation method;",
            "switching to separable joint density estimation")
        sepa <- TRUE
    }
    if (is.null(sepa)){ sepa = !supp; }

    ## initialize
    # force lists for easing manipulations
    if (!is.list(f)){ f = list(f) }
    nf <- length(f)
    n <- nrow(X)
    CDMSA <- lapply(f, function(fi){
                  list(XY = sapply(X, function(Xi){NA}),
                       XX = sapply(X, function(Xi){NA}),
                       YY = NA)
             }) 
    # check if `dest_kernel` takes weights as argument
    weight_kernel <- !is.null(w) && "w" %in% names(formals(dest_kernel))

    ## precompute Y kernel matrix and probabilities
    # kernel
    if (!is.double(Y)){ KY <- class_kernel_matrix(Y) }
    else if (cond == "conditional" && weight_kernel){
        KY <- dest_kernel(Y, w = w)
    }else{ KY <- dest_kernel(Y) }
    # proba
    if (is.null(w) || cond == "hybrid"){ PY <- rowMeans(KY) }
    else{ PY <- KY%*%w }
    # remove kernel if not needed anymore
    if (!sepa && is.double(Y) && all(sapply(X, is.double))){ remove(KY) }

    if (norm){ ## CDM(Y, Y)
        # compute product and joint distribution and ratios
        if (!supp){ # distributions are represented by n-by-n matrices
            if (is.null(w) || cond == "hybrid"){
                R <- (KY%*%t(KY)/n)/(PY%o%PY)
            }else{ # weights modulate along the row
                R <- (scal_mul_col(KY, w)%*%t(KY))/(PY%o%PY)
            }
        }else if (!is.double(Y)){
            R <- PY # PY*PY/PY
        }else{
            if(sepa){ KYY <- KY*KY }
            else{
                if (cond == "conditional" && weight_kernel){
                    KYY <- dest_kernel(cbind(Y, Y), w = w)
                }else{
                    KYY <- dest_kernel(cbind(Y, Y))
                }
            }
            if (cond == "hybrid" || is.null(w)){
                R <- (PY*PY)/rowMeans(KYY)
            }else{
                R <- (PY*PY)/(KYY%*%w)
            }
            remove(KYY)
        }

        # apply the functions and compute Monte-Carlo integrals
        for (fi in 1:nf){
            fR <- f[[fi]](R)
            if (is.null(w)){ CDMSA[[fi]]$YY <- mean(fR) }
            else if (supp){ CDMSA[[fi]]$YY <- w%*%fR }
            else{ # integrate against the product of weighted distributions
                CDMSA[[fi]]$YY <- w%*%fR%*%w
            }
        }
    } #endif norm

    ### iterate over each factor
    for (i in 1:length(X)){
        Xi <- X[[i]]

        ## X kernel matrix and probabilities
        # kernel
        if (!is.double(Xi)){ KX <- class_kernel_matrix(Xi) }
        else if (cond == "conditional" && weight_kernel){
            KX <- dest_kernel(Xi, w = w)
        }else{ KX <- dest_kernel(Xi) }
        # proba
        if (is.null(w) || cond == "hybrid"){ PX <- rowMeans(KX) }
        else{ PX <- KX%*%w }

        if (norm){ ## CDM(X, X)
            # compute product and joint distribution and ratios
            if (!supp){ # distributions are represented by n-by-n matrices
                if (is.null(w) || cond == "hybrid"){
                    R <- (KX%*%t(KX)/n)/(PX%o%PX)
                }else{ # weights modulate along the row
                    R <- (scal_mul_col(KX, w)%*%t(KX))/(PX%o%PX)
                }
            }else if (!is.double(Xi)){
                R <- PX # PX*PX/PX
            }else{
                if(sepa){ KXX <- KX*KX }
                else{
                    if (cond == "conditional" && weight_kernel){
                        KXX <- dest_kernel(cbind(Xi, Xi), w = w)
                    }else{
                        KXX <- dest_kernel(cbind(Xi, Xi))
                    }
                }
                if (cond == "hybrid" || is.null(w)){
                    R <- (PX*PX)/rowMeans(KXX)
                }else{
                    R <- (PX*PX)/(KXX%*%w)
                }
                remove(KXX)
            }

            # apply the functions and compute Monte-Carlo integrals
            for (fi in 1:nf){
                fR <- f[[fi]](R)
                if (is.null(w)){ CDMSA[[fi]]$XX[i] <- mean(fR) }
                else if (supp){ CDMSA[[fi]]$XX[i] <- w%*%fR }
                else{ # integrate against the product of weighted distributions
                    CDMSA[[fi]]$XX[i] <- w%*%fR%*%w
                }
            }
        } #endif norm

        ## CDM(X, Y)
        # compute product and joint distribution and ratios
        if (!supp){ # distributions are represented by n-by-n matrices
            if (is.null(w) || cond == "hybrid"){
                R <- (KX%*%t(KY)/n)/(PX%o%PY)
            }else{ # weights modulate along the row
                R <- (scal_mul_col(KX, w)%*%t(KY))/(PX%o%PY)
            }
        }else{
            if (!is.double(Xi) || !is.double(Y) || sepa){ KXY <- KX*KY }
            else{
                if (cond == "conditional" && weight_kernel){
                    KXY <- dest_kernel(cbind(Xi, Y), w = w)
                }else{
                    KXY <- dest_kernel(cbind(Xi, Y))
                }
            }
            if (cond == "hybrid" || is.null(w)){
                R <- (PX*PY)/rowMeans(KXY)
            }else{
                R <- (PX*PY)/(KXY%*%w)
            }
            remove(KXY)
        }

        # apply the functions and compute Monte-Carlo integrals
        for (fi in 1:nf){
            fR <- f[[fi]](R)
            if (is.null(w)){ CDMSA[[fi]]$XY[i] <- mean(fR) }
            else if (supp){ CDMSA[[fi]]$XY[i] <- w%*%fR }
            else{ # integrate against the product of weighted distributions
                CDMSA[[fi]]$XY[i] <- w%*%fR%*%w
            }
        }
    }#endfor i

    ## reformat output
    # remove noncomputed normalization
    if (!norm){ CDMSA <- lapply(CDMSA, function(CDMf){ CDMf$XY }) }
    # undo transformation to list
    if (nf == 1){ CDMSA <- CDMSA[[1]] }
    return(CDMSA)
}

fKL <- function(t){ return(-log(t)) }

fTV <- function(t){ return(abs(t - 1)) }

fHellinger <- function(t){ return((sqrt(t) - 1)^2) }

fPearson <- function(t){ return((t - 1)^2) }

fNeyman <- function(t){ return(((t - 1)^2)/t) }
