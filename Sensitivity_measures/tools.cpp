# include <Rcpp.h>
using namespace Rcpp;

// used for lambdas for functions computing ordering indices
// [[Rcpp::plugins(cpp11)]]

#include <limits> // for the machine precision epsilon
#include <cstring> // for memcpy
#include <cmath> // sqrt, exp, cos, sin, ...

// [[Rcpp::plugins(openmp)]]
#ifdef _OPENMP
    #include <omp.h>
    // rough minimum number of operations per thread
    #define MIN_OPS_PER_THREADS 1000
#endif

// nops is a rough estimation of the total number of operations 
// max_threads is the maximum number of jobs which can be performed in 
// parallel
int compute_num_threads(const int nops, const int max_threads)
{
#ifdef _OPENMP
    const int m = (omp_get_num_procs() < max_threads) ?
        omp_get_num_procs() : max_threads;
    int n = 1 + (nops - MIN_OPS_PER_THREADS)/MIN_OPS_PER_THREADS;
    return (n < m) ? n : m;
#else
    return 1;
#endif
}

// compute distance matrix between rows of the input matrix
// dist can be "Euclidean" (l2-norm), "Euclidean2" (square l2-norm),
// or "Chebyshev" (max-norm); other should be added of course!
// [[Rcpp::export]]
NumericMatrix dist_matrix_Rcpp(const NumericMatrix &X, const std::string &dist)
{
    const int n = X.nrow(), p = X.ncol(), np = n*p;
    int i, j, k;
    const double *Xi, *Xj;
    double dif, d;
    NumericMatrix D(n, n);

    const int nthreads = compute_num_threads(n*n, n);
    if (dist == "Euclidean2"){
        #pragma omp parallel for private(i, j, k, Xi, Xj, dif, d) \
            schedule(static) num_threads(nthreads)
        for (i = 1; i < n; i++){
            Xi = X.begin() + i;
            for (j = 0; j < i; j++){
                Xj = X.begin() + j;
                d = 0.;
                for (k = 0; k < np; k += n){
                    dif = Xi[k] - Xj[k];
                    d += dif*dif;
                }
                D(i,j) = D(j,i) = d;
            }
            D(i, i) = 0.;
        }
    }else if (dist == "Euclidean"){
        #pragma omp parallel for private(i, j, k, Xi, Xj, dif, d) \
            schedule(static) num_threads(nthreads)
        for (i = 1; i < n; i++){
            Xi = X.begin() + i;
            for (j = 0; j < i; j++){
                Xj = X.begin() + j;
                d = 0.;
                for (k = 0; k < np; k += n){
                    dif = Xi[k] - Xj[k];
                    d += dif*dif;
                }
                D(i,j) = D(j,i) = sqrt(d);
            }
            D(i, i) = 0.;
        }
    }else{ // Chebyshev
        #pragma omp parallel for private(i, j, k, Xi, Xj, dif, d) \
            schedule(static) num_threads(nthreads)
        for (i = 1; i < n; i++){
            Xi = X.begin() + i;
            for (j = 0; j < i; j++){
                Xj = X.begin() + j;
                d = 0.;
                for (k = 0; k < np; k += n){
                    dif = Xi[k] - Xj[k];
                    if (dif > d){ d = dif; }
                    else if (dif < -d){ d = -dif; }
                }
                D(i,j) = D(j,i) = d;
            }
            D(i, i) = 0.;
        }
    }
    D(0, 0) = 0.;
    return D;
}

// exponential function, parallelized along columns
// [[Rcpp::export]]
NumericMatrix exp_Rcpp(const NumericMatrix &X)
{
    const int m = X.nrow(), n = X.ncol();
    int i, j;
    const double *Xj;
    double *Yj;
    NumericMatrix Y(m,n);

    const int nthreads = compute_num_threads(m*n, n);
    #pragma omp parallel for private(i, j, Xj, Yj) schedule(static) \
        num_threads(nthreads) 
    for (j = 0; j < n; j++){
        Xj = X.begin() + m*j;
        Yj = Y.begin() + m*j;
        for (i = 0; i < m; i++){ Yj[i] = exp(Xj[i]); }
    }
    return Y;
}

// cosine function, parallelized along columns
// [[Rcpp::export]]
NumericMatrix cos_Rcpp(const NumericMatrix &X)
{
    const int m = X.nrow(), n = X.ncol();
    int i, j;
    const double *Xj;
    double *Yj;
    NumericMatrix Y(m,n);

    const int nthreads = compute_num_threads(m*n, n);
    #pragma omp parallel for private(i, j, Xj, Yj) schedule(static) \
        num_threads(nthreads) 
    for (j = 0; j < n; j++){
        Xj = X.begin() + m*j;
        Yj = Y.begin() + m*j;
        for (i = 0; i < m; i++){ Yj[i] = cos(Xj[i]); }
    }
    return Y;
}

// sine function, parallelized along columns
// [[Rcpp::export]]
NumericMatrix sin_Rcpp(const NumericMatrix &X)
{
    const int m = X.nrow(), n = X.ncol();
    int i, j;
    const double *Xj;
    double *Yj;
    NumericMatrix Y(m,n);

    const int nthreads = compute_num_threads(m*n, n);
    #pragma omp parallel for private(i, j, Xj, Yj) schedule(static) \
        num_threads(nthreads) 
    for (j = 0; j < n; j++){
        Xj = X.begin() + m*j;
        Yj = Y.begin() + m*j;
        for (i = 0; i < m; i++){ Yj[i] = sin(Xj[i]); }
    }
    return Y;
}

// sqrt function, parallelized along columns
// [[Rcpp::export]]
NumericMatrix sqrt_Rcpp(const NumericMatrix &X)
{
    const int m = X.nrow(), n = X.ncol();
    int i, j;
    const double *Xj;
    double *Yj;
    NumericMatrix Y(m,n);

    const int nthreads = compute_num_threads(m*n, n);
    #pragma omp parallel for private(i, j, Xj, Yj) schedule(static) \
        num_threads(nthreads) 
    for (j = 0; j < n; j++){
        Xj = X.begin() + m*j;
        Yj = Y.begin() + m*j;
        for (i = 0; i < m; i++){ Yj[i] = sqrt(Xj[i]); }
    }
    return Y;
}

// scalar multiplication, parallelized along columns
// [[Rcpp::export]]
NumericMatrix scal_mul_col_Rcpp(const NumericMatrix &X, const NumericVector &a)
{
    const int m = X.nrow(), n = X.ncol();
    int i, j;
    const double *Xj;
    double aj, *Yj;
    NumericMatrix Y(m, n);

    const int nthreads = compute_num_threads(m*n, n);
    #pragma omp parallel for private(i, j, aj, Xj, Yj) schedule(static) \
        num_threads(nthreads) 
    for (j = 0; j < n; j++){
        Xj = X.begin() + m*j;
        Yj = Y.begin() + m*j;
        aj = a[j];
        for (i = 0; i < m; i++){ Yj[i] = aj*Xj[i]; }
    }
    return Y;
}

// scalar multiplication, parallelized along rows
// [[Rcpp::export]]
NumericMatrix scal_mul_row_Rcpp(const NumericMatrix &X, const NumericVector &a)
{
    const int m = X.nrow(), n = X.ncol(), mn = m*n;
    int i, j;
    const double *Xi;
    double ai, *Yi;
    NumericMatrix Y(m, n);

    const int nthreads = compute_num_threads(m*n, n);
    #pragma omp parallel for private(i, j, ai, Xi, Yi) schedule(static) \
        num_threads(nthreads) 
    for (i = 0; i < m; i++){
        Xi = X.begin() + i;
        Yi = Y.begin() + i;
        ai = a[i];
        for (j = 0; j < mn; j += m){ Yi[j] = ai*Xi[j]; }
    }
    return Y;
}

// scalar multiplication, along rows and parallelized along columns
// [[Rcpp::export]]
NumericMatrix scal_mul_row_col_Rcpp(const NumericMatrix &X, const NumericVector &a)
{
    const int m = X.nrow(), n = X.ncol();
    int i, j;
    const double *Xj;
    double aj, *Yj;
    NumericMatrix Y(m, n);

    const int nthreads = compute_num_threads(m*n, n);
    #pragma omp parallel for private(i, j, aj, Xj, Yj) schedule(static) \
        num_threads(nthreads) 
    for (j = 0; j < n; j++){
        Xj = X.begin() + m*j;
        Yj = Y.begin() + m*j;
        aj = a[j];
        for (i = 0; i < m; i++){ Yj[i] = a[i]*aj*Xj[i]; }
    }
    return Y;
}

// get ordering indices for each column of the input matrix
// [[Rcpp::export]]
IntegerMatrix order_col_Rcpp(const NumericMatrix &X)
{
    const int m = X.nrow(), n = X.ncol();
    int i, j, *Ij;
    const double *Xj;
    IntegerMatrix I(m, n);

    const int nthreads = compute_num_threads(m*n, n);
    #pragma omp parallel for private(i, j, Xj, Ij) schedule(static) \
        num_threads(nthreads)
    for (j = 0; j < n; j++){
        // R matrices are stored in column-major format
        Xj = X.begin() + m*j;
        Ij = I.begin() + m*j;
        for (i = 0; i < m; i++){ Ij[i] = i; }
        // the following requires C++11 for the lambda function
        std::sort(Ij,  Ij + m, [Xj](int k, int l){ return (Xj[k] < Xj[l]); });
        for (i = 0; i < m; i++){ Ij[i]++; } // convert to R-indices
    }
    return I;
}

// give ranking of the columns of the input matrix by ascending order
// [[Rcpp::export]]
NumericMatrix rank_col_Rcpp(const NumericMatrix &X)
{
    const int m = X.nrow(), n = X.ncol();
    const double *Xj;
    int i, j, k, *buf;
    double r, *Rj;
    NumericMatrix R(m, n);

    const int nthreads = compute_num_threads(m*n, n);
    #pragma omp parallel private(i, j, k, r, buf, Rj, Xj) \
        num_threads(nthreads)
    {
        buf = (int*) malloc(sizeof(int)*m);
        #pragma omp for schedule(static)
        for (j = 0; j < n; j++){
            // R matrices are stored in column-major format
            // compute ordering indices
            Xj = X.begin() + m*j;
            for (i = 0; i < m; i++){ buf[i] = i; }
            // the following requires C++11 for the lambda function
            std::sort(buf,  buf + m, [Xj](int k, int l){ 
                return (Xj[k] < Xj[l]); });
            // convert C-indices to rank
            Rj = R.begin() + m*j;
            // for (i = 0; i < m; i++){ Rj[buf[i]] = (double) (i + 1); }
            // take ties into account
            for (i = 0; i < m;){ 
                k = i + 1;
                while (k < m && Xj[buf[k]] == Xj[buf[i]]){ k++; }
                r = (i + 1 + k)/2.;
                for(; i < k; i++){ Rj[buf[i]] = r; }
            }
        }
        free(buf);
    }
    return R;
}

// give weighted ranking of the columns of the input matrix
// weighted rank is the cumulative sum of the weights ordered according to X
// [[Rcpp::export]]
NumericMatrix weight_rank_col_Rcpp(const NumericMatrix &X, const NumericVector &w)
{
    const int m = X.nrow(), n = X.ncol();
    const double *Xj;
    int i, j, k, *buf;
    double r, rk, *Rj;
    NumericMatrix R(m, n);

    const int nthreads = compute_num_threads(m*n, n);
    #pragma omp parallel private(i, j, k, r, rk, buf, Rj, Xj) \
        num_threads(nthreads)
    {
        buf = (int*) malloc(sizeof(int)*m);
        #pragma omp for schedule(static)
        for (j = 0; j < n; j++){
            // R matrices are stored in column-major format
            // compute ordering indices
            Xj = X.begin() + m*j;
            for (i = 0; i < m; i++){ buf[i] = i; }
            // the following requires C++11 for the lambda function
            std::sort(buf,  buf + m, [Xj](int k, int l){ 
                return (Xj[k] < Xj[l]); });
            // convert C-indices to weighted rank
            Rj = R.begin() + m*j;
            r = 0.;
            // for (i = 0; i < m; i++){ Rj[buf[i]] = (r += w[buf[i]]); }
            // take ties into account
            for (i = 0; i < m;){ 
                rk = r + w[buf[i]];
                k = i + 1;
                while (k < m && Xj[buf[k]] == Xj[buf[i]]){ rk += w[buf[k++]]; }
                r = (r + w[buf[i]] + rk)/2.;
                for(; i < k; i++){ Rj[buf[i]] = r; }
                r = rk;
            }
        }
        free(buf);
    }
    return R;
}

// give the k-th smallest element of each column of the input matrix
// [[Rcpp::export]]
NumericVector kth_elmnt_col_Rcpp(const NumericMatrix &X, const int k)
{
    const int m = X.nrow(), n = X.ncol();
    if (k < 1){ stop("k must be a strictly positive integer"); }
    if (k > m){ stop("k must be less than number of rows"); }
    int j;
    double *buf;
    NumericVector Xkth(n);

    const int nthreads = compute_num_threads(m*n, n);
    #pragma omp parallel private(j, buf) num_threads(nthreads)
    {
        buf = (double*) malloc(sizeof(double)*m);
        #pragma omp for schedule(static)
        for (j = 0; j < n; j++){
            // R matrices are stored in column-major format
            // copy the column so that sorting does not modify input
            std::memcpy(buf, X.begin() + m*j, sizeof(double)*m);
            std::nth_element(buf,  buf + k - 1, buf + m);
            Xkth(j) = buf[k - 1];
        }
        free(buf);
    }
    return Xkth;
}

// naive quickselect algorithm with weights on the rank
// k is given as a double precision float (usually cast from an integer)
// min(w) <= k <= n and sum(w) = 1
double weight_kth_elmnt(const double *X, const double k, const int n, const double *w, int *I)
{
    int i, ip, ii, idx, idx_inf = 0, idx_sup = n - 1;
    double xp, xi, wk, wk_inf = 0.; // wk_inf is weighted rank of inf boundary
    for (i = 0; i < n; i++){ I[i] = i; }
    // precompute ratio and account for cumulative rounding errors
    const double k_n = k/n - std::numeric_limits<double>::epsilon()*n;

    while (true){
        ip = I[idx_sup];
        xp = X[ip];
        i = idx = idx_inf;
        wk = wk_inf + w[ip];
        // permute X according to comparisons with xp
        // avoid useless swap as long as i = idx; cannot exceed idx_sup
        ii = I[i];
        while (X[ii] < xp || (X[ii] == xp && wk < k_n))
            { idx++; wk += w[ii]; ii = I[++i]; }
        // i != idx, now we must swap
        for (; i < idx_sup; i++){
            xi = X[ii = I[i]];
            if (xi < xp || (xi == xp && wk < k_n))
                { I[i] = I[idx]; I[idx++] = ii; wk += w[ii]; }
        }
        // xp has weighted rank wk
        // modify range according to k, and swap pivot if necessary
        // recall that xp has weighted rank k/n if the cumulative ordered 
        // weights up to index ip reaches k/n, while the cumulative ordered 
        // weights up ip - 1 is below k; that is to say:
        //     wk >= k/n    and    wk - w[ip] < k/n
        if (wk < k_n){ // swap pivot 
            I[idx_sup] = I[idx]; I[idx] = ip;
            idx_inf = idx + 1;
            wk_inf = wk;
        }else if (wk - w[ip] < k_n){ return xp; }
        else{ idx_sup = idx - 1; } // no need to swap
    }
}

// give for each column of the input matrix, the element of weighted rank k/m
// weighted rank is the cumulative sum of the weights ordered according to X
// 1 <= k <= n and sum(w) = 1, m is the number of rows of the matrix
// [[Rcpp::export]]
NumericVector weight_kth_elmnt_col_Rcpp(const NumericMatrix &X, const double
    k, const NumericVector &w)
{
    const int m = X.nrow(), n = X.ncol();
    if (k <= 0.){ stop("k must be a strictly positive integer"); }
    if (k > m){ stop("k must be less than number of rows"); }
    int j, *buf;
    NumericVector Xkth(n);

    const int nthreads = compute_num_threads(m*n, n);
    #pragma omp parallel private(j, buf) num_threads(nthreads)
    {
        buf = (int*) malloc(sizeof(int)*m);
        #pragma omp for schedule(static)
        for (j = 0; j < n; j++){
            // R matrices are stored in column-major format
            Xkth(j) = weight_kth_elmnt(X.begin() + m*j, k, m, w.begin(), buf);
        }
        free(buf);
    }
    return Xkth;
}
