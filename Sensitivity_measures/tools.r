RcppAvailable <- "Rcpp" %in% installed.packages()[,"Package"]
# RcppAvailable <- FALSE # for testing without Rcpp
if (RcppAvailable){
    library("Rcpp")
    cat("Checking C++ sources:\n")
    Sys.setenv(PKG_CXXFLAGS = "-fopenmp")
    Sys.setenv(PKG_LIBS = "-fopenmp")
    sourceCpp("tools.cpp", verbose = TRUE)
}else{
    cat("Rcpp package not found in the system; tools will not make use of it.\n")
    cat("Install Rcpp to unleash the true power of R with C++\n")
    cat("While you're at it, ensure also OpenMP is available to you system.\n")
}

                          #-------------------------#
                          #  numerical computation  #
                          #-------------------------#
if (RcppAvailable){
    exp_R <- exp_Rcpp
    cos_R <- cos_Rcpp
    sin_R <- sin_Rcpp
    sqrt_R <- sqrt_Rcpp
    scal_mul_row <- scal_mul_row_Rcpp
    scal_mul_col <- scal_mul_col_Rcpp
    scal_mul_row_col <- scal_mul_row_col_Rcpp
}else{
    exp_R <- exp
    cos_R <- cos
    sin_R <- sin
    sqrt_R <- sqrt
    scal_mul_row <- function(X, w){ (w%o%rep.int(1, ncol(X)))*X }
    scal_mul_col <- function(X, w){ X*(rep.int(1, nrow(X))%o%w) }
    scal_mul_row_col <- function(X, w){ (w%o%w)*X }
}

                                 #-----------#
                                 #  sorting  #
                                 #-----------#
if (RcppAvailable){
    order_col <- order_col_Rcpp
}else{
    order_col <- function(X)
    #
    #       order_col(X)
    #
    # get permutation indices sorting in ascending order each column of the 
    # input matrix
    #
    # Hugo Raguet 2017
    { apply(X, 2, order) }
}

if (RcppAvailable){
    rank_col <- rank_col_Rcpp
}else{
    rank_col <- function(X)
    #
    #       rank_col(X)
    #
    # ranks of the elements of each column of the input matrix, in ascending
    # order
    #
    # Hugo Raguet 2017
    { apply(X, 2, rank) }
}

if (RcppAvailable){
    kth_elmnt_col <- kth_elmnt_col_Rcpp
}else{
    kth_elmnt_col <- function(X, k)
    #
    #       kth_rank_col(X, k)
    #
    # give the k-th element of each column of the input matrix
    #
    # INPUT:
    #   X - n-by-d matrix, n observations in dimension d
    #   k - the desired rank
    #
    # Hugo Raguet 2017
    { apply(X, 2, sort, partial = k)[k,] }
}

if (RcppAvailable){
    weight_kth_elmnt_col <- weight_kth_elmnt_col_Rcpp
}else{
    weight_kth_elmnt_col <- function(X, k, w)
    #
    #       weight_kth_elmnt_col(X, k, w)
    #
    # give the element of weighted rank k/n of each column of the input matrix
    # weighted rank is the cumulative sum of the weights ordered according to X
    #
    # INPUT:
    #   X - n-by-d matrix, n observations in dimension d
    #   k - the desired rank
    #   w - vector of length n summing to unity, weighting the observations.
    #
    # Hugo Raguet 2017
    {
        k_n <- k/nrow(X) - .Machine$double.eps*nrow(X) # for machine precision
        O <- apply(X, 2, order)
        I <- apply(O, 2, function(Oj){ match(TRUE, cumsum(w[Oj]) > k_n) })
        seqidx <- (0:(ncol(X)-1))*nrow(X)
        return(X[seqidx + O[seqidx + I]])
    }
}

                               #---------------#
                               #  basic stats  #
                               #---------------#
std_col <- function(X, w = NULL)
# 
#   std_col(X, w = NULL)
#
# compute (weighted) standard deviation of each column of input matrix X
# when no weights are given, denominator of variance is n
#
# INPUT:
#   X - n-by-d matrix, n observations in dimension d
#   w - vector of length n summing to unity, weighting the observations.
# 
# Hugo Raguet 2017
{
    if (is.null(w)){
        return(sqrt(colMeans(sweep(X, 2, colMeans(X), "-")^2)))
    }else{
        return(sqrt(w%*%(sweep(X, 2, w%*%X, "-")^2)))
    }
}

median_col <- function(X, w = NULL)
# 
#       median_col(X, w = NULL)
#
# compute (weighted) median of each column of input matrix X
# when between to values, highest value is given
#
# INPUT:
#   X - n-by-d matrix, n observations in dimension d
#   w - vector of length n summing to unity, weighting the observations.
# 
# Hugo Raguet 2017
{
    if (is.null(w)){ return(kth_elmnt_col(X, ceiling(nrow(X)/2))) }
    else{ return(weight_kth_elmnt_col(X, nrow(X)/2, w)) }
}

iqr_std_col <- function(X, w = NULL)
# 
#       iqr_std_col(X, w = NULL)
#
# compute (weighted) interquartile range of each column of input matrix X,
# multiplied by 0.7413 for a robust, asymptotically normal consistent
# estimation of the (weighted) standard deviation
#
# INPUT:
#   X - n-by-d matrix, n observations in dimension d
#   w - vector of length n summing to unity, weighting the observations.
# 
# Hugo Raguet 2017
{
    if (is.null(w)){
        Q1 <- ceiling(nrow(X)/4)
        Q3 <- ceiling(3*nrow(X)/4)
        iqr <- kth_elmnt_col(X, Q3) - kth_elmnt_col(X, Q1)
    }else{
        Q1 <- nrow(X)/4
        Q3 <- 3*nrow(X)/4
        iqr <- weight_kth_elmnt_col(X, Q3, w) - weight_kth_elmnt_col(X, Q1, w)
    }
    return(0.7413*iqr)
}

mad_std_col <- function(X, w = NULL)
# 
#       mad_std_col(X, w = NULL)
#
# compute (weighted) median of absolute deviation of each column of input
# matrix X, multiplied by 1.4826 for a robust, asymptotically normal
# consistent estimation of the (weighted) standard deviation
#
# INPUT:
#   X - n-by-d matrix, n observations in dimension d
#   w - vector of length n summing to unity, weighting the observations.
# 
# Hugo Raguet 2017
{ return(1.4826*median_col(abs(sweep(X, 2, median_col(X, w), "-")), w)) }


                               #--------------#
                               #  transforms  #
                               #--------------#
scale_col <- function(X, S = std_col, w = NULL, center = FALSE)
#
#       scale_col(X, S = std_col, w = NULL, center = FALSE)
#
# scale the columns of X according to S and weights w
#
# INPUTS:
#   X - n-by-d matrix, n observations in dimension d
#   S - numeric value, or vector of length d, or a function to estimate it
#       on the data, default to empirical standard deviation; if setting
#       weights on the distribution of the observations changes the scaling,
#       the function should take the weights as argument name "w" (see e.g.
#       std_col)
#   w - vector of length n summing to unity, weighting the observations
#   center - logical; if TRUE, the columns means are subtracted
#
# Hugo Raguet 2017
{
    if (is.function(S)){
        if ("w" %in% names(formals(S))){ S <- S(X, w = w) }   
        else{ S <- S(X) }
    }
    if (length(S) == 1){
        if (S != 1){ X <- scal_mul_col(X, rep.int(1/S, ncol(X))) }
    }else{
        X <- scal_mul_col(X, 1/S)
    }
    if (!center){ return(X) }
    else if (is.null(w)){ return(sweep(X, 2, colMeans(X), "-")) }
    else{ return(sweep(X, 2, w%*%X, "-")) }
}

if (RcppAvailable){
    copula_col <- function(X, w = NULL)
    #
    #       copula_col(X, w = NULL)
    #
    # compute (weighted) empirical copula of columns of X
    #
    # INPUT:
    #   X - or n-by-d matrix, n observations in dimension d
    #   w - vector of length n summing to unity, weighting the observations.
    #
    # Use Rcpp with OpenMP parallelization
    #
    # Hugo Raguet 2017
    {
        if (is.null(w)){ return(rank_col_Rcpp(X)/nrow(X)) }
        else{ return(weight_rank_col_Rcpp(X, w)) }
    }
}else{
    copula_col <- function(X, w = NULL)
    #
    #       copula_col(X, w = NULL)
    #
    # compute (weighted) empirical copula of X
    #
    # INPUT:
    #   X - n-by-d matrix, n observations in dimension d
    #   w - vector of length n summing to unity, weighting the observations.
    #
    # Hugo Raguet 2017
    {
        if (is.null(w)){
            return(apply(X, 2, rank)/nrow(X))
        }else{
            O <- apply(X, 2, order)
            return(apply(O, 2, function(Oj){ 
                R <- numeric(n); R[Oj] = 1L:nrow(X) # get rank
                return(cumsum(w[Oj])[R])
            }))
        }
    }
}

                      #--------------------------------#
                      #  distance and kernel matrices  #
                      #--------------------------------#
if (RcppAvailable){
    dist_matrix <- dist_matrix_Rcpp
}else{
    dist_matrix <- function(X, distance = "Euclidean")
    #
    #       dist_matrix(X, distance = "Euclidean")
    #
    # compute cross distances of the row vectors of X
    # INPUT:
    #   X - vector of length n or n-by-d matrix, n observations in dimension d
    #   distance - string specifying the distance to use:
    #       Euclidean2: dist(Xi, Xj) = ||Xi - Xj||_2^2 = sum_k (Xik - Xjk)^2
    #       Euclidean:  dist(Xi, Xj) = ||Xi - Xj||_2
    #       Chebyshev:  dist(Xi, Xj) = ||Xi - Xj||_inf = max_k |Xik - Xjk|
    # OUTPUT:
    #   n-by-n matrix, Dij = dist(Xi, Xj)
    #
    # Hugo Raguet 2017
    {
        if (distance == "Euclidean" || distance == "Euclidean2"){
            DX <- X%*%t(X)
            X2 <- diag(DX) # square norm of each observation
            if (distance == "Euclidean"){
                return(sqrt(outer(X2, X2, "+") - 2*DX))
            }else{
                return(outer(X2, X2, "+") - 2*DX)
            }
        }else{ # Chebyshev
            return(as.matrix(dist(X, "maximum")))
        }
    }
}

rbf_kernel_matrix <- function(X, SX = std_col, w = NULL, SD = 1, f = fGaussian)
#
#       rbf_kernel_matrix(X, SX = std_col, w = NULL, SD = 1, f = fGaussian)
#
# compute radial basis function kernel matrix of the row vectors of X,
# with a diagonal bandwidth matrix given by SX and SD
#
#       Kij = f(<(Xi - Xj), (S^-2) (Xi - Xj)>)
#           = f(sum_k (X_i,k - X_j,k)^2/(S_k^2))
#
# INPUTS:
#   X  - n-by-d matrix, n observations in dimension d
#   f  - basis function taking the square cross distances as arguments
#        provided in this file:
#        fGaussian:  r2 -> exp(-r2/2)     for Gaussian kernel (default)
#        fLaplace:   r2 -> exp(-sqrt(r2)) for Laplace kernel
#        fCauchy:    r2 -> 1/(1 + r2)     for Cauchy kernel
#        f_ratio_quadratic: r2 <- 1 - r2/(r2 + 1)
#                                         for rational quadratic kernel
#        f_imult_quadric:   r2 -> 1/sqrt(r2 + 1)
#                                         for inverse multi quadric kernel
#        fMatern3_2: r2 -> (1 + sqrt(3)*r)*exp(-sqrt(3)*r),
#            r = sqrt(r2);                for Matern 3/2 kernel
#        fMatern5_3: r2 -> (1 + sqrt(5)*r + (5/3)*r2)*exp(-sqrt(5)*r),
#            r = sqrt(r2);                for Matern 5/3 kernel
#        f_dCov:     r2 -> sqrt(r2)       emulates distance covariance
#                                         kernel (see details in f_dCov)
#        The provided functions are not normalized (i.e. the kernel does
#        not integrate to unity).
#        TODO: Epanechnikov, and Epanechnikov to the power of the dimension,
#        are usefull for density estimations, if properly normalized.
#   SX - the normalization on the _data_; numeric value, or vector of length d,
#        or a function to estimate it on the data, default to empirical
#        standard deviations; if setting weights on the distribution of the
#        observations changes the scaling, the function should take the weights
#        as argument name "w" (see e.g. std_col).
#   w  - vector of length n summing to unity, weighting the observations.
#   SD - the normalization on the _distance matrix_; numeric value, or vector
#        of length d, or a function to estimate it on the data, default to 1
#
# Hugo Raguet 2017
{ return(f(scale_col(dist_matrix(scale_col(X, SX, w), "Euclidean2"), SD))) }

fGaussian <- function(r2) # for Gaussian kernel
{ exp_R(-r2/2) }

fLaplace <- function(r2) # for Laplace kernel
{ exp_R(-sqrt_R(r2)) }

fCauchy <- function(r2) # for Cauchy kernel
{ 1/(r2 + 1) }

f_ratio_quadratic <- function(r2) # for rational quadratic kernel
{ 1 - r2/(r2 + 1) }

f_imult_quadric <- function(r2) # for inverse multi quadric kernel
{ 1/sqrt_R(r2 + 1) }

f_Matern3_2 <- function(r2) # for Matern 3/2 kernel
{ r <- sqrt_R(r2); (1 + sqrt(3)*r)*exp_R(-sqrt(3)*r) }

f_Matern5_3 <- function(r2) # for Matern 5/3 kernel
{ r <- sqrt_R(r2); (1 + sqrt(5)*r + (5/3)*r2)*exp_R(-sqrt(5)*r) }

f_dCov <- function(r2) # for emulating distance covariance kernel when used
# with kQDM; the *actual* kernel is defined in dCov_kernel_matrix
{ sqrt_R(r2) }

dCov_kernel_matrix <- function(X, S = 1, w = NULL)
#
#       dCov_kernel_matrix(X, S = std_col, w = NULL)
#
# compute kernel matrix of the row vectors of X for the kernel leading to
# distance covariance quadratic dependence measure
#
#       Kij = 1/2*(||Xi|| + ||Xj||) - ||Xi - Xj||
#
# INPUTS:
#   X - n-by-d matrix, n observations in dimension d
#   S - numeric value, or vector of length d, or a function to estimate it
#       on the data, default to median of absolute deviations; if setting
#       weights on the distribution of the observations changes the scaling,
#       the function should take the weights as argument name "w" (see e.g.
#       std_col).
#   w - vector of length n summing to unity, weighting the observations.

#
# Hugo Raguet 2017
{
    X <- scale_col(X, S, w)
    NX <- sqrt(rowSums(X^2))/2
    return(outer(NX, NX, '+') - dist_matrix(X, "Euclidean"))
}

class_kernel_matrix <- function(X, norm = FALSE)
#
#       class_kernel_matrix(X, norm = FALSE)
#
# compute kernel matrix for the simplest categorical kernel (i.e. defined over
# a space of finite number of classes)
#       Kij = 1{Xi = Xj} / ni
# where ni is a normalization factor depending on the class i (which is also j
# when relevent, hence kernel is symmetrical)
#
# INPUT:
#   X    - factor or vector of length n, n observations
#   norm - tells if the kernel should be normalized, for each class, by the
#          number of elements in the class
#
# Hugo Raguet 2017
{
    K <- outer(as.vector(X), as.vector(X), "==")
    storage.mode(K) <- "double"
    if (norm){ K <- scal_mul_row(K, 1/rowSums(K)) }
    return(K)
}

                           #----------------------#
                           #  density estimation  #
                           #----------------------#
Silverman_diagonal_bandwith <- function(X, w = NULL)
#
#       Silverman_diagonal_bandwith(X, w = NULL)
#
# Silverman's (1986) rule of thumb for diagonal bandwidth,
#       hi = .9*min(std, 1.4826*mad)/n^(1/(d+4))
# where std and mad are the empirical standard-deviation and median of absolute
# deviation, respectively; nota: original formulation uses interquartile range
#
# INPUT:
#   X - n-by-d matrix, n observations in dimension d
#   w - vector of length n summing to unity, weighting the observations
#
# Hugo Raguet 2017
{ .9*pmin(std_col(X, w), mad_std_col(X, w))/(nrow(X)^(1/(ncol(X) + 4))) }

Gauss_Silverman_kernel_matrix <- function(X, w = NULL)
#
#       Gauss_Silverman_kernel_matrix(X, w = NULL)
#
# compute Gaussian kernel matrix for density estimate
#       Kij = exp(-<Xi - Xj, (S^-2) (Xi - Xj)>/2)/(|S|*sqrt(2*pi)^d)
# with diagonal bandwidth according to Silverman's (1986) rule of thumb.
#
# INPUT:
#   X - vector of length n or n-by-d matrix, n observations in dimension d
#   w - vector of length n summing to unity, weighting the observations
#
# Hugo Raguet 2017
{
    S <- Silverman_diagonal_bandwith(X, w) 
    return(rbf_kernel_matrix(X, S)/(((2*pi)^(ncol(X)/2))*prod(S)))
}

Gauss_adaptive_kernel_matrix <- function(X, al = 1/2, w = NULL)
#
#       Gauss_adaptive_kernel_matrix(X, al = 1/2, w = NULL)
#
# compute adaptve Gaussian kernel matrix for density estimate; working
# linewise (kernel is adaptative, thus nonsymmetric)
#
#      Kij = exp(-<Xi - Xj, (S^-2) (Xi - Xj)>/(2 a_j))/(|S|*(a_j*sqrt(2*pi))^d)
#
# with adaptive bandwidth (see Silverman's, 1986):
#   1. the 'pilot' density f is estimated by Gaussian kernel with diagonal
#      bandwidth set following Silverman's rule of thumb.
#   2. the adaptive bandwidth is set as si <- si*ai, where
#      a_j = (fj/g)^(- al), and
#         fj is the pilot density estimate at observation j
#         g  is the geometric mean of the fj's
#         al is a tuning parameter 0 < al <= 1, the higher the more adaptive
#           Breiman, Meisel and Purcell (1977) propose 1/d while
#           Abramson (1982) advocates 1/2.
#
# INPUT:
#   X  - n-by-d matrix, n observations in dimension d
#   al - adaptive power, 0 < al <= 1
#   w - vector of length n summing to unity, weighting the observations
#
# Hugo Raguet 2017
{
    S <- Silverman_diagonal_bandwith(X, w) 
    K <- rbf_kernel_matrix(X, S, f = identity)
    # get log of pilot estimate (hence normalizing constant unnecessary)
    a <- log(rowSums(exp_R(-K/2)))
    a <- exp(al*(mean(a) - a)) # geometric mean and power
    K <- exp_R(-scal_mul_col(K, 1/(2*a)))
    return(scal_mul_col(K, 1/(((2*pi)^(ncol(X)/2))*prod(S)*a^ncol(X))))
}

trunc_knn_kernel_matrix <- function(X, Xmin = 0, Xmax = 1, w = NULL)
#
#       trunc_knn_kernel_matrix <- function(X, Xmin = 0, Xmax = 1, w = NULL)
#
# create kernel matrix for density estimation equivalent to truncated
# k-nearest-neighbors method with Chebyshev distances; working linewise
# (kernel is adaptative, thus nonsymmetric)
#       Kij = 1{dist(Xi, Xj) <= dki}/Vi
# where
#       Xi is the i-th observation
#       dki is the distance between Xi and its k-th nearest neighbors
#       Vi is the volume of the intersection between the cuboid determined by
#       Xmin and Xmax, and the Chebyshev ball (cuboid) of radius dki
#       k is set following Silverman (1986, p.99) without further details,
#           k = ceiling(n^(4/(d+4)))
#
# INPUT:
#   X - n-by-d matrix, n observations in dimension d
#   Xmin, Xmax - scalars or vectors of length d defining range of X
#        Xmin = 0 and Xmax = 1 corresponds to copula density estimates
#   w - vector of length n summing to unity, weighting the observations
#
# Hugo Raguet 2017
{
    # compute k and distance matrix
    k <- ceiling(nrow(X)^(4/(ncol(X)+4)))
    DX <- dist_matrix(X, "Chebyshev")
    # get k-th nearest neighbors distance
    if (is.null(w)){ dk <- kth_elmnt_col(DX, k) }
    else{ dk <- weight_kth_elmnt_col(DX, k, w) }
    # truncate distances
    if (length(Xmin) == 1){ X <- pmin(X - Xmin, Xmax - X) }
    else{ X <- pmin(sweep(X, 2, Xmin, "-"), sweep(-X, 2, Xmax, "+")) }
    # compute the volume of the cuboid enclosing the nearest neighbors
    V <- apply(sweep(X, 1, dk, function(x, d){ d + pmin(d, x) }), 1, prod)
    # flag the k-th nearest neighbors with inverse of the volume
    return(scal_mul_row(sweep(DX, 1, dk, "<="), 1/V))
}
