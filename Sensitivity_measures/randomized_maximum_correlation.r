# requires tools.r

RMC_SA <- function(X, Y, fX = transf_norm_fun(copula_col_one, k = 
    round(sqrt(nrow(X))), f = sine), fY = NULL, w = NULL, adjustedR2 =
    TRUE)
#
#       RMC_SA(X, Y, fX = transf_norm_fun(copula_col_one, k =
#           round(sqrt(nrow(X))), f = csine), fY = NULL, w = NULL, 
#           adjustedR2 = TRUE)
#
# conduct sensitivity analysis by measuring dependence between input factors
# Xi and output Y with randomized maximum correlation (also called randomized
# dependence coefficient, see reference below)
#
# INPUTS: 
#   X  - n-by-d data frame, n observations of d input factors, each as n-by-pi
#        matrix, in dimension pi.
#   Y  - n-by-q matrix, n model outputs of dimension q
#   fX - function computing randomized "nonlinear projections" of the
#        obervations for X, mapping n-by-p matrix to n-by-kX matrix; or list of
#        d such functions specifying a different nonlinear projection for each
#        input factor; if setting weights on the distribution of the
#        observations changes the transformations, the function should take the
#        weights as argument name "w" (see e.g. transf_norm_fun).
#   fY - function computing randomized "nonlinear projections" of the
#        obervations for Y, default to the first nonlinear projection for X
#   w  - weights modifying the distribution of the observations, useful for
#        conditional sensitivity; vector of length n summing to unity;
#   adjustedR2 - since maximum correlation is estimated through canonical
#        correlation, it is strongly positively biased. If `adjustedR2` is
#        TRUE, the function returns the SQUARED canonical correlation
#        coefficient adjusted according to the number n of observations and
#        the total number kX + kY of regressors. The formula used is that of
#        Wherry (1931) developped for multiple regression, adapted for
#        canonical correlation by replacing the number of "dependent variables"
#        (usually `M` in the reference paper) by (kX + kY - 1).
#        Note that while canonical correlation is guaranteed to be positive,
#        its adjusted square is not.
#
# OUTPUT:
#   vector of length d, randomized maximum correlation between Y and each input
#   factor Xi
#
# Reference: D. Lopez-Paz, P. Hennig and P. B. Schölkopf, The Randomized
# Dependence Coefficient, NIPS 2013
#
# Hugo Raguet 2017
{
    # initialization
    d <- length(X)
    n <- nrow(X)
    if (!is.list(fX)){ fX <- rep(list(fX), d) }
    if (is.null(fY)){ fY <- fX[[1]] }
    # preprocessing on Y
    if ("w" %in% names(formals(fY))){ Y <- fY(Y, w = w) }
    else{ Y <- fY(Y) }
    if (is.null(w)){
        Y <- sweep(Y, 2, colMeans(Y), "-")
    }else{
        sqrw <- sqrt(w)
        Y <- scal_mul_col(sweep(Y, 2, w%*%Y, "-"), sqrw)
    }
    QY <- qr(Y)
    kY <- QY$rank
    QY <- qr.qy(QY, diag(1, n, kY))

    # compute RMC
    RMC <- sapply(X, function(Xi){ NA })
    for (i in 1L:d){
        if ("w" %in% names(formals(fX[[i]]))){ Xi <- fX[[i]](X[[i]], w = w) }
        else{ Xi <- fX[[i]](X[[i]]) }
        if (is.null(w)){
            Xi <- sweep(Xi, 2, colMeans(Xi), "-")
        }else{
            Xi <- scal_mul_col(sweep(Xi, 2, w%*%Y, "-"), sqrw)
        }
        QX <- qr(Xi)
        kX <- QX$rank
        CC <- qr.qty(QX, QY)[1L:kX, , drop = FALSE]
        RMC[i] <- svd(CC, nu = 0L, nv = 0L)$d[1]
        if (adjustedR2){
            if (n > kX + kY){
                RMC[i] <- 1 - (1 - RMC[i]^2)*((n - 1)/(n - (kX + kY - 1)))
            }else{
                RMC[i] <- NaN
            }
        }
    }
    return(RMC)
}

csine <- function(X)
# function (cos(2 pi x), sin(2 pi <x,th>))
{ X <- 2*pi*X; return(cbind(cos_R(X), sin_R(X))) }

sine <- function(X)
# function sin(2 pi x)
{ return(sin_R(2*pi*X)) }

logistic <- function(X)
# function 1/(1 + exp(- x))
{ return(1/(1 + exp_R(-X))) }

copula_col_one <- function(X, w = NULL)
# copula transform augmented by a constant unit coordinate
{ return(cbind(copula_col(X, w = w), rep.int(1, nrow(X)))) }

scale_col_one <- function(X, S = std_col, w = NULL, center = TRUE)
# copula transform augmented by a constant unit coordinate
{
    return(cbind(scale_col(X, S = S, w = w, center = center),
        rep.int(1, nrow(X))))
}

transf_norm_fun <- function(T, k, f)
#
#       transf_norm_fun(T, k, f)
#
# nonlinear transformation and projection composing:
#   transformation on X (copula, scaling, ...)
#   linear projection against normally distributed vectors with variance
#       inverse of the dimension of the transformation of X (1/q)
#   nonlinear functional
#
# INPUT:
#   T - function n-by-p matrix to n-by-q matrix transforming the data;
#       if setting weights on the distribution of the observations changes the
#       transformations, the function should take the weights as argument name
#       "w" (see e.g. copula_col).
#   k - parameter tuning the number of projections per dimension of X
#   f - nonlinear functional
#
# OUTPUT: 
#   function mapping n-by-p matrix to n-by-k' matrix, where k' depends on
#   functional f, dimensin p, and parameter k
{
    if ("w" %in% names(formals(T))){
        return(function(X, w = NULL){
            p <- ncol(X)
            TX <- T(X, w = w)
            q <- ncol(TX)
            Th <- matrix(rnorm(q*(k*p)), nrow = q, ncol = (k*p))/sqrt(q)
            return(f(TX%*%Th))
        })
    }else{
        return(function(X){
            p <- ncol(X)
            TX <- T(X)
            q <- ncol(TX)
            Th <- matrix(rnorm(q*(k*p)), nrow = q, ncol = (k*p))/sqrt(q)
            return(f(TX%*%Th))
        })
    }
}
