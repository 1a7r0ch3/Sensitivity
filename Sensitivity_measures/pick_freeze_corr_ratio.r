pick_freeze_corr_ratio_SA <- function(Y)
#
#       pick_freeze_corr_ratio_SA(Y)
#
# pick-and-freeze estimator of the generalized Sobol' indices using the estimator
# of Monod et al. (2006) on a Saltelli plan
#
# INPUTS: 
#   Y - Saltelli plan; see Saltelli_plan function
#
# OUTPUTS:
#   list containing the following elements
#   S1 - first-order indices, numeric vector
#   ST - total-order indices, numeric vector
#
# Hugo Raguet 2017
{
    d <- length(Y$Yi)
    S1 <- ST <- numeric(length = d)
    for (i in 1L:d){
        # first order indices
        S1[i] <- corr_ratio_Monod(Y$Y1, Y$Yi[[i]])
        # total order indices
        ST[i] <- 1 - corr_ratio_Monod(Y$Y2, Y$Yi[[i]])
    }
    names(S1) <- names(ST) <- names(Y$Yi)
    return(list(S1 = S1, ST = ST))
}

Saltelli_plan <- function(X, f, verbose = FALSE)
#
#       Saltelli_plan(X, f)
#
# Create a Saltelli plan from a Monte-Carlo sample for estimating Sobol'
# indices, allowing to estimate first- and total-orders indices with only
# n*(d + 2) model calls instead of n*(2d + 1) for classic Sobol' Monte-Carlo
#
# INPUTS: 
#   X - 2n-by-d data frame, 2n observations of d input factors
#   f - model function, taking as argument several observations of X as a
#       n-by-d data frame (see above), and returning a vector of length n or an
#       n-by-q matrix, where q is the output dimensionality
#
# OUTPUTS:
#   list containing the following elements:
#   Y1 - model evaluated on the first half of the observations, vector of
#        length n or n-by-q matrix
#   Y2 - model evaluated on the second half of the observations, vector of
#        length n or n-by-q matrix
#   Yi - n-by-d data frame of d vectors of length n or n-by-q matrices,
#        containing for each input factor i in {1,...,d}, the model evaluated
#        on the input factor i taken from the first half of the observations
#        and the other input factors taken from the second half
#
# Hugo Raguet 2017
{
    d <- length(X)
    n <- floor(nrow(X)/2)
    if (verbose){ cat("compute first", 2*n, "model outputs...") }
    Y1 <- f(X[1L:n,])
    Y2 <- f(X[(n+1):(2*n),])
    if (verbose){ cat("done.\n") }
    Yi <- data.frame(matrix(nrow = n, ncol = d))
    Xi <- X[(n+1):(2*n),]
    for (i in 1L:d){
        if (verbose){ cat("compute ", n, " model outputs for ",
                "indices of input #", i, "/", d, "...", sep = "") }
        Xi[,i] <- X[1L:n,i] # input factor i from the first half
        Yi[[i]] <- f(Xi)
        Xi[,i] <- X[(n+1):(2*n),i] # back from the second half
        if (verbose){ cat("done.\n") }
    }
    names(Yi) <- names(X)
    return(list(Y1 = Y1, Y2 = Y2, Yi = Yi))
}

corr_ratio_Monod <- function(Y, Yi)
#
#       corr_ratio_Monod(Y, Yi)
#
# estimate correlation ratio of a (group of) given input factor(s) based on
# Monod et al. (2006), but sum the variances along possibly several outputs,
# like Gamboa et al. (2014) (which amounts to average correlation ratios for
# all outputs weighted by each output's variance)
#
# INPUTS: 
#   Y  - model evaluated at n observations, vector of length n or n-by-q matrix
#   Yi - model evaluated at n observations different than above for all but the
#        (group of) input factor(s) of interest, vector of length n or n-by-q
#        matrix
#
# Hugo Raguet 2017
{
    if (is.vector(Y)){ # unidimensional output
        Y2 <- (mean((Y + Yi))^2)/4
        VCE <- mean(Y*Yi) - Y2
        V <- mean(Y^2 + Yi^2)/2 - Y2
        return(VCE/V)
    }else{
        Y2 <- colMeans(Y + Yi)^2/4
        VCE <- colMeans(Y*Yi) - Y2
        V <- colMeans(Y^2 + Yi^2)/2 - Y2
        return(sum(VCE)/sum(V))
    }
}
