      #-----------------------------------------------------------------#
      #  script for testing basic tools of global sensitivity analysis  #
      #-----------------------------------------------------------------#
# Hugo Raguet 2017
# check availability of needed packages and source files below
source("Sensitivity_analysis/sensitivity_measures.r", chdir = TRUE)
source("Sensitivity_analysis/Tests/test_models.r")

                           #----------------------#
                           #  general parameters  #
                           #----------------------#
test_sensitivity_package <- FALSE # for sensitivity package;
# WARNING : Csiszar divergence and kQDM (HSIC) are very long to compute; the
# latter sometimes crashes; seems also to have some troubles with Sobol indices
# with Saltelli plan, some run fail with '"LG_estimator" not resolved from
# current namespace (sensitivity)'
if (test_sensitivity_package){
    library("sensitivity")
    library("ks") # for kernel density estimates (sensiFdiv)
}
# maximum number of observations allowed for estimation of full Csiszar
# divergence dependence measure (this is O(n^3))
max_n_Cdiv_full <- 2000 
# test for full dependence and INdependence in high dimension; makes sense only
# of all input are numeric (no factors)
add_full_and_indpdt <- FALSE

samples <- c(50L, 200L, 500L, 1000L) # sample sizes to test
repetitions <- 100L # number of independent repetitions for statistics
                    # can be different for each sample size

verbose = TRUE # print info on process and results
.verbose = verbose # variable for function `vinfo`
# number of significant digits and scientific notation when printing
# intermediate results on terminal
options(digits = 3, scipen = -3)

# directory to print results
resdir <- "Sensitivity_analysis/Results/"

                            #--------------------#
                            #  model definition  #
                            #--------------------#
##  select model below or preload your own; see test_models.r
Ishigami_Homma_model(a = 5, b = .1)
if (FALSE){ ######## CODE BELOW THIS LINE IS NOT EXECUTED #####################
Sobol_g_model(a = NULL, d = 6)
awgn_indpdt_model(s = .1)
model_from_observations(X, Y, name = "custom", sampling = "observations")
    # the following can be used for bootstrap estimator of the variance of the
    # sensitivity measures; however, note that a dedicated implementation for
    # each sensitivity measure, taking into account the fact that the data used
    # are always the same (e.g. distance matrices, or ranking for copula
    # transforms, should be computed once and for all; although special
    # attention should still be given to ties for the latter), would usually be
    # faster
model_from_observations(X, Y, name = "custom", sampling = "bootstrap")
if (max(samples) > nrow(model$Xparam$X)){
    warning("sampling over up to", max(samples), "observations, but only",
        nrow(model$Xparam$X), "observations provided.")
}
} ################## CODE ABOVE THIS LINE IS NOT EXECUTED #####################

vinfo(cat, "\n\t\t###  Test with", model$name, "model  ###\n")
vinfo(cat, "parameters for input factors:", num2str(model$Xparam), "\n")
vinfo(cat, "parameters for model function:", num2str(model$fparam), "\n")
vinfo(cat, "model sampling scheme:", model$sampling, "\n")

                               #--------------#
                               #  processing  #
                               #--------------#
# initialize data frame storing results
repetitions <- rep_len(repetitions, length.out = length(samples))
names(repetitions) <- lapply(samples, function(n){ paste("n = ", n, sep = "")
    })
allSA <- lapply(repetitions, function(nrep){ data.frame(matrix(nrow = nrep,
    ncol = 0)) })

for (n in samples){
    nn <- paste("n = ", n, sep = "")
    nrep <- repetitions[[nn]]
    vinfo(cat, "\n\t\t###  compute sensitivity measures for", nrep,
        "different sample(s) of size", n, " ###\n")
    progress_message <- ""

for (r in 1L:nrep){
    .verbose = verbose
    vinfo(cat, rep.int("\b", nchar(progress_message)), sep = "")
    progress_message = paste("\nrepetition #", r, "/", nrep, sep ="")
    vinfo(cat, progress_message)
    if (r == 1){ progress_message = "" }
    else{ .verbose = FALSE }

    vinfo(cat, "\nsampling ", model$sampling, "... ", sep = "")
    timer <- proc.time()
    XY <- model$sample(n)
    X <- XY$X; Y <- XY$Y; remove(XY)
    # ensure input factors and output are matrices
    for (i in 1L:length(X)){
        if (is.null(dim(X[[i]]))){ dim(X[[i]]) <- c(length(X[[i]]), 1) }
    }
    if (is.null(dim(Y))){ dim(Y) <- c(length(Y), 1) }

    if (add_full_and_indpdt){
        # test for full dependence and independence in high dimension
        X[[model$Xparam$d+1]] <- as.matrix(X)
        X[[model$Xparam$d+2]] <- X[[model$Xparam$d+1]][sample.int(n),]
        names(X)[ncol(X)-c(1,0)] <- c("X", "I") 
    }
    timer <- proc.time() - timer
    vinfo(cat, "done.\nelapsed time:", timer[3], "s\n")

###  Sobol indices  ###
if (!is.null(model[["f"]])){ # needs ability to construct Saltelli plan

vinfo(cat, "\ncompute pick-and-freeze correlation ratio indices, first- and",
    "total-order, Saltelli plan, Monod method... ")
timer <- proc.time()
SA <- pick_freeze_corr_ratio_SA(Saltelli_plan(X[,1L:model$Xparam$d], model$f))
timer <- proc.time() - timer
vinfo(cat, "done.\nelapsed time:", timer[3], "s\n")
name <- "PF corr2 1"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA$S1)
name <- "PF corr2 T"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA$ST)

# sanity check on multidimensionnal output equivalent to unidimensional
vinfo(cat, "\ncompute pick-and-freeze multidimensional correlation ratio",
    "indices, first- and total-order, Saltelli plan, Monod method... ")
timer <- proc.time()
SA <- pick_freeze_corr_ratio_SA(Saltelli_plan(X[,1L:model$Xparam$d], 
    function(X){ cbind(model$f(X), model$f(X)) }))
timer <- proc.time() - timer
vinfo(cat, "done.\nelapsed time:", timer[3], "s\n")
name <- "PF corr2 multd 1"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA$S1)
name <- "PF corr2 multd T"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA$ST)

if (test_sensitivity_package){
    vinfo(cat, "\ncompute pick-and-freeze correlation ratio indices with",
        "sensitivity package, first- and total-order, Saltelli plan, Monod",
        "method... ")
    timer <- proc.time()
    n_2 <- floor(n/2)
    SA <- sobolSalt(model = model$f, X1 = X[1:n_2,1L:model$Xparam$d], X2 = 
        X[(n_2+1):(2*n_2),1L:model$Xparam$d], scheme = "A")
    timer <- proc.time() - timer
    vinfo(cat, "done.\nelapsed time:", timer[3], "s\n")
    name <- "PF corr2 sensi 1"
    allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
        data.matrix(SA$S))
    name <- "PF corr2 sensi T"
    allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
        data.matrix(SA$T))
}

} # endif is.null(f)

###  kernel quadratic dependence measure  ###
vinfo(cat, "\ncompute kernel quadratic dependence measure sensitivity",
    "analysis with normalizations, Gaussian kernel... ")
timer <- proc.time()
SA <- kQDM_SA(X, Y, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done.\nelapsed time:", timer[3], "s\n")
name <- "kQDM Gauss std coor"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA$XY)
name <- "nkQDM Gauss std coor"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
    SA$XY/sqrt(SA$XX*SA$YY))

vinfo(cat, "\ncompute kernel quadratic dependence measure sensitivity",
    "analysis with normalizations, Gaussian kernel over copula transforms... ")
timer <- proc.time()
SA <- kQDM_SA(dfapply(X, copula_col), copula_col(Y), kX = function(X){ 
    rbf_kernel_matrix(X, SX = 1, f = fGaussian) }, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done.\nelapsed time:", timer[3], "s\n")
name <- "kQDM copu Gauss std coor"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA$XY)
name <- "nkQDM copu Gauss std coor"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
    SA$XY/sqrt(SA$XX*SA$YY))

vinfo(cat, "\ncompute kernel quadratic dependence measure sensitivity",
    "analysis with normalizations, distance covariance kernel... ")
timer <- proc.time()
# emulate kernel with simple distance
SA <- kQDM_SA(X, Y, kX = function(X){ rbf_kernel_matrix(X, SX = 1,
    f = f_dCov) }, norm = TRUE)
# check that dCov is kQDM with suitable kernel
# SA <- kQDM_SA(X, Y, kX = dCov_kernel_matrix, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done.\nelapsed time:", timer[3], "s\n")
name <- "dCov"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA$XY)
name <- "dCor"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
    SA$XY/sqrt(SA$XX*SA$YY))

if (test_sensitivity_package){ # seems to get stuck with (not so) many samples
    vinfo(cat, "\ncompute kernel quadratic dependence measure sensitivity",
        "analysis with sensitivity package, Gaussian and distance covariance",
        " kernels... ")
    timer <- proc.time()
    SA <- lapply(list(Gauss = 'rbf', dCor = 'dcov'), function(k){
                 return(tell(sensiHSIC(X = X, kernelX = k, kernelY = k), Y))
             })
    timer <- proc.time() - timer
    vinfo(cat, "done.\nelapsed time:", timer[3], "s\n")
    name <- "nkQDM Gauss sensi"
    allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
        data.matrix(SA[["Gauss"]]$S))
    name <- "dCor sensi"
    allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r,
        data.matrix(SA[["dCor"]]$S))
}

###  Csiszar divergence dependence measure  ###
# set different functions defining Csiszar divergence
if (test_sensitivity_package){
    flist <- list(KL = fKL, TV = fTV, Chi2 = fNeyman)
}else{
    flist <- list(MI = fKL, TV = fTV, "Neyman Chi2" = fNeyman, Hellinger = 
        fHellinger)
}

vinfo(cat, "\ncompute support Csiszar divergence sensitivity analysis with",
    " normalizations, Gaussian kernel density estimation... ")
timer <- proc.time()
SA <- CDM_SA(X, Y, f = flist, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done.\nelapsed time:", timer[3], "s\n")
for (fun in names(flist)){
    name <- paste("sCDM", fun, "Gauss Silverman")
    allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r,
        SA[[fun]]$XY)
    name <- paste("nsCDM", fun, "Gauss Silverman")
    allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
        SA[[fun]]$XY/SA[[fun]]$XX)
}

vinfo(cat, "\ncompute support Csiszar divergence sensitivity analysis with", 
    "normalizations, separable Gaussian kernel density estimation... ")
timer <- proc.time()
SA <- CDM_SA(X, Y, f = flist, sepa = TRUE, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done.\nelapsed time:", timer[3], "s\n")
for (fun in names(flist)){
    name <- paste("sCDM", fun, "Gauss sepa")
    allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r,
        SA[[fun]]$XY)
    name <- paste("nsCDM", fun, "Gauss sepa")
    allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r,
        SA[[fun]]$XY/SA[[fun]]$XX)
}

vinfo(cat, "\ncompute support Csiszar divergence sensitivity analysis with",
     "normalizations, k-nearest-neighbors density estimation... ")
timer <- proc.time()
SA <- CDM_SA(dfapply(X, copula_col), copula_col(Y), f = flist, dest_kernel = 
    trunc_knn_kernel_matrix, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done.\nelapsed time:", timer[3], "s\n")
for (fun in names(flist)){
    name <- paste("sCDM", fun, "trunc knn copu")
    allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r,
        SA[[fun]]$XY)
    name <- paste("nsCDM", fun, "trunc knn copu")
    allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
        SA[[fun]]$XY/SA[[fun]]$XX)
}

if (n > max_n_Cdiv_full){
    vinfo(cat, "number of observations (", n, ") is greater than ",
        "allowed limit (", max_n_Cdiv_full, ") for 'full' ",
        "Csiszar divergence dependence measure\n", sep = "")
}else{
    vinfo(cat, "\ncompute full Csiszar divergence sensitivity analysis with",
        "normalizations, Gaussian kernel density estimation... ")
    timer <- proc.time()
    SA <- CDM_SA(X, Y, f = flist, supp = FALSE, norm = TRUE)
    timer <- proc.time() - timer
    vinfo(cat, "done.\nelapsed time:", timer[3], "s\n")
    for (fun in names(flist)){
        name <- paste("CDM", fun, "Gauss sepa")
        allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r,
            SA[[fun]]$XY)
        name <- paste("nCDM", fun, "Gauss sepa")
        allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r,
            SA[[fun]]$XY/SA[[fun]]$XX)
    }
}

if (test_sensitivity_package){
    vinfo(cat, "\ncompute Csiszar divergence dependence measure with",
        "sensitivity package... ")
    timer <- proc.time()
    SA <- sensiFdiv(X = X, fdiv = names(flist))
    tell(SA, Y)
    timer <- proc.time() - timer
    vinfo(cat, "done.\nelapsed time:", timer[3], "s\n")
    for (fun in names(flist)){
        name <- paste("CDM", fun, "sensi")
        allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
            data.matrix(SA$S[[fun]]))
    }
}

###  randomized maximum correlation  ###
vinfo(cat, "\ncompute randomized maximum correlation, copula transform and",
    "sine nonlinearities... ")
timer <- proc.time()
SA <- RMC_SA(X, Y)
timer <- proc.time() - timer
vinfo(cat, "done.\nelapsed time:", timer[3], "s\n")
name <- "RMC2 copu sine"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA)

vinfo(cat, "\nestimate correlation ratio with randomized maximum correlation,",
    "copula transform and sine nonlinearities... ")
timer <- proc.time()
SA <- RMC_SA(X, Y, fY = identity)
timer <- proc.time() - timer
vinfo(cat, "done.\nelapsed time:", timer[3], "s\n")
name <- "RMC corr2 copu sine"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA)

} # endfor r in 1L:nrep

                         #--------------------------#
                         #  print and save results  #
                         #--------------------------#
.verbose = verbose
vinfo(cat, "\n\t\t###  average sensitivity measures over", nrep,
    "different sample(s) of size", n, " ###\n")
# if there is too many input factors, try the transpose
vinfo(print, do.call(rbind, lapply(allSA[[nn]], colMeans)))

} #enfor n in samples

if (model$sampling %in% c("observations", "bootstrap", "permutation")){
    expname = num2str(list(model$name, model$sampling), len = 1)
}else{
    expname = num2str(list(model$name, model$Xparam, model$fparam, 
        model$sampling), len = 1)
}
expname <- paste("GSA_", gsub("[ :]+", "_", expname), sep ="")
expdir <- file.path(resdir, expname)

vinfo(cat, "saving results in ", expdir, "... ", sep = "")
if (!dir.exists(resdir)){ dir.create(resdir) }
if (!dir.exists(expdir)){ dir.create(expdir) }

save(model, allSA, file = file.path(expdir, "model_SA.rdata"))

pdf(file.path(expdir, "boxplots_summary.pdf"), 
    width = length(allSA)*max(ncol(X)/3 + 1, 2.5), height = 2)
#        layout            vert X label       bot left top  right
par(mfcol=c(1, length(allSA)), las = 2, mai = c(.4, 0.4, 0.5, 0.2))
for (method in names(allSA[[1]])){
    for (nn in names(allSA)){
        boxplot(allSA[[nn]][[method]])
        title(paste(method, "\n", nn, "rep =", repetitions[[nn]]))
    }
}
dev.off()
vinfo(cat, "done.\n")

# copy and paste the following for commenting out big sections of code
if (FALSE){ ######## CODE BELOW THIS LINE IS NOT EXECUTED #####################
} ################## CODE ABOVE THIS LINE IS NOT EXECUTED #####################
