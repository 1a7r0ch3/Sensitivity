 #----------------------------------------------------------------------------#
 #  script for testing permutation tests and bootstrap on global, target and  #
 #  conditional sensitivity analysis                                          #
 #----------------------------------------------------------------------------#
# the following can be used for bootstrap estimator of the variance of the
# sensitivity measures, or permutation tests of significance; however, note
# that a dedicated implementation for each sensitivity measure, taking into
# account the fact that the data used are always the same (e.g. distance
# matrices, or ranking for copula transforms, should be computed once and for
# all; although special attention should still be given to ties for the
# copula transforms), would usually be faster
#
# The critical domain is {Y > cr} for a certain critical value cr
# Hugo Raguet 2017
# check availability of needed packages and source files below
source("Sensitivity_analysis/sensitivity_measures.r", chdir = TRUE)
source("Sensitivity_analysis/Tests/test_models.r")
 
                           #----------------------#
                           #  general parameters  #
                           #----------------------#
samples <- c(200L, 1000L, 10000L) # sample sizes to test
repetitions <- 100L # number of independent repetitions for statistics
                    # (actually one sample and `repetitions` bootstraps or
                    # permutations)
                    # can be different for each sample size

verbose = TRUE # print info on process and results
.verbose = verbose # variable for function `vinfo`
# number of significant digits and scientific notation when printing
# intermediate results on terminal
options(digits = 3, scipen = -3)

# parameter for soft Y transform, the higher the smoother
# ws = exp(-dist(Y,C)/(weight_smoothness*sd(Y)))
weight_smoothness <- 1/5
# minimum number of critical observations for a sample to be acceptable
min_cr_obs <- 5

# directory to print results
resdir <- "Sensitivity_analysis/Results/"

# initialize data frame storing results
repetitions <- rep_len(repetitions, length.out = length(samples))
names(repetitions) <- lapply(samples, function(n){ paste("n = ", n, sep = "")
    })
allSA <- lapply(repetitions, function(nrep){ data.frame(matrix(nrow = nrep,
    ncol = 0)) })

for (n in samples){
    nn <- paste("n = ", n, sep = "")
    nrep <- repetitions[[nn]]

                           #-----------------------#
                           #  create observations  #
                           #-----------------------#
Ishigami_Homma_model(a = 5, b = .1, sampling = "space filling")
# Sobol_g_model(a = c(0, 1, 9, 99), sampling = "space filling")

vinfo(cat, "\n\t\t###  Generate sample of size ", n, " with ", model$sampling,
    " sampling... ", sep = "")
XY <- model$sample(n)
X <- XY$X; Y <- XY$Y; remove(XY)
vinfo(cat, "done.  ###\n")

# include parameters in the model name
model$name <- num2str(list(model$name, model$Xparam, model$fparam), len = 1)

                            #--------------------#
                            #  model definition  #
                            #--------------------#
##  select model below or preload your own (in which case, comment the above);
##  see test_models.r
model_from_observations(X, Y, name = model$name, sampling = "permutation")
# model_from_observations(X, Y, name = model$name, sampling = "bootstrap")
    model$cr <- NULL # critical value automatically selected as a quantile
    crq <- .9 # quantile for automatic selection of a critical value
    if ((1 - crq)*n < min_cr_obs){ 
        stop("minimum ", min_cr_obs, " critical observations requested,",
            " but only ", floor((1 - crq)*n), " available when critical domain", 
            " is defined by ", crq, "-th quantile of ", n, " observations.")
    }

vinfo(cat, "\n\t\t###  Test with", model$name, "model  ###\n")
vinfo(cat, "parameters for input factors:", num2str(model$Xparam), "\n")
vinfo(cat, "parameters for model function:", num2str(model$fparam), "\n")
vinfo(cat, "model sampling scheme:", model$sampling, "\n")

# compute a suitable critical value as quantile
if (is.null(model$cr)){
    vinfo(cat, "compute critical value as quantile", num2str(crq), "over", 
        max(samples), "values... ")
    model$cr <- quantile(Y, crq)
    vinfo(cat, "done.\n")
}
vinfo(cat, "critical value is", num2str(model$cr), "\n")

                               #--------------#
                               #  processing  #
                               #--------------#
vinfo(cat, "\n\t\t###  compute sensitivity measures for", nrep,
    "different sample(s) of size", n, " ###\n")
progress_message <- ""

for (r in 1L:nrep){
    .verbose = verbose
    vinfo(cat, rep.int("\b", nchar(progress_message)), sep = "")
    progress_message = paste("\nrepetition #", r, "/", nrep, sep ="")
    vinfo(cat, progress_message)
    if (r == 1){ progress_message = "" }
    else{ .verbose = FALSE }

    vinfo(cat, "\nsampling ", model$sampling, "... ", sep = "")
    timer <- proc.time()
    ncr <- 0
    while (ncr < min_cr_obs){
        if (r == nrep){ # last one uses actual observations
            X <- model$Xparam$X
            Y <- model$fparam$Y
        }else{
            XY <- model$sample(n)
            X <- XY$X; Y <- XY$Y; remove(XY)
        }
        # ensure input factors and output are matrices
        for (i in 1L:length(X)){
            if (is.null(dim(X[[i]]))){ dim(X[[i]]) <- c(length(X[[i]]), 1) }
        }
        if (is.null(dim(Y))){ dim(Y) <- c(length(Y), 1) }

        ###  compute transformation and weights according to target domain  ###
        # hard Y transform
        Yh <- Y >= model$cr
        ncr <- sum(Yh)
        wh <- as.vector(Yh/ncr)
        # smooth Y transform
        Ys <- exp(pmin(Y - model$cr, 0)/(weight_smoothness*sd(Y)))
        ws <- as.vector(Ys/sum(Ys))
    }
    timer <- proc.time() - timer
    vinfo(cat, "done; elapsed time:", timer[3], "s\n")
    vinfo(cat, "\nnumber of critical observations:", ncr, "\n")

###  Sobol' indices  ###
if (!is.null(model[["f"]])){ # needs ability to construct Saltelli plan

vinfo(cat, "\ncompute pick-and-freeze correlation ratio indices, first- and",
    "total-order, Saltelli plan, Monod method:\n")
vinfo(cat, "global... ")
timer <- proc.time()
Y_plan <- Saltelli_plan(X, model$f)
SA <- pick_freeze_corr_ratio_SA(Y_plan)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "PF corr2 1"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA$S1)
name <- "PF corr2 T"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA$ST)

vinfo(cat, "hard target... ")
timer <- proc.time()
# hard Y transform for target SA
Yh_plan <- lapply(Y_plan, function(Y){
       Yh <- as.matrix(Y) >= model$cr
       storage.mode(Yh) <- "double"
       if (is.data.frame(Y)){ Yh <- data.frame(Yh) }
       return(Yh)
   })
SA <- pick_freeze_corr_ratio_SA(Yh_plan)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "PF corr2 1 hard target"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA$S1)
name <- "PF corr2 T hard target"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA$ST)

vinfo(cat, "smooth target... ")
timer <- proc.time()
# smooth Y transform for target SA
sdY <- sd(do.call(base::c, lapply(Y_plan, as.matrix)))
Ys_plan <- lapply(Y_plan, function(Y){
       Ys <- exp(pmin(as.matrix(Y) - model$cr, 0)/(weight_smoothness*sdY))
       if (is.data.frame(Y)){ Ys <- data.frame(Ys) }
       return(Ys)
   })
SA <- pick_freeze_corr_ratio_SA(Ys_plan)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "PF corr2 1 smooth target"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA$S1)
name <- "PF corr2 T smooth target"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA$ST)

vinfo(cat, "hard hybrid... ")
timer <- proc.time()
# hard weighted Y for hybrid SA
y0 <- sum(do.call(base::c, lapply(Yh_plan, as.matrix))*do.call(base::c,
    lapply(Y_plan, as.matrix)))
y0 <- y0/sum(do.call(base::c, lapply(Yh_plan, as.matrix)))
Ywh_plan <- lapply(setNames(nm = names(Y_plan)), function(Y){ 
        Yh_plan[[Y]]*Y_plan[[Y]] + (1 - Yh_plan[[Y]])*y0
    })
SA <- pick_freeze_corr_ratio_SA(Ywh_plan)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "PF corr2 1 hard hybrid"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA$S1)
name <- "PF corr2 T hard hybrid"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA$ST)

vinfo(cat, "smooth hybrid... ")
timer <- proc.time()
# smooth weighted Y for hybrid SA
y0 <- sum(do.call(base::c, lapply(Ys_plan, as.matrix))*do.call(base::c,
    lapply(Y_plan, as.matrix)))
y0 <- y0/sum(do.call(base::c, lapply(Ys_plan, as.matrix)))
Yws_plan <- lapply(setNames(nm = names(Y_plan)), function(Y){ 
        Ys_plan[[Y]]*Y_plan[[Y]] + (1 - Ys_plan[[Y]])*y0
    })
SA <- pick_freeze_corr_ratio_SA(Yws_plan)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "PF corr2 1 smooth hybrid"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA$S1)
name <- "PF corr2 T smooth hybrid"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA$ST)

# clear memory
remove(Y_plan, Yh_plan, Ys_plan, Ywh_plan, Yws_plan)

} # endif is.null(f)

###  kernel quadratic dependence measure  ###
vinfo(cat, "\ncompute normalized kernel quadratic dependence measure,",
    "Gaussian kernel, bandwidth determined by empirical standard deviation of",
    "each coordinate:\n")
vinfo(cat, "global... ")
timer <- proc.time()
SA <- kQDM_SA(X, Y, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "nkQDM Gauss std coor"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
    SA$XY/sqrt(SA$XX*SA$YY))

vinfo(cat, "hard target (categorical kernel on Y)... ")
timer <- proc.time()
storage.mode(Yh) <- "logical"
SA <- kQDM_SA(X, Yh, kY = class_kernel_matrix, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "nkQDM Gauss hard target"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
    SA$XY/sqrt(SA$XX*SA$YY))

vinfo(cat, "smooth target... ")
timer <- proc.time()
SA <- kQDM_SA(X, Ys, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "nkQDM Gauss smooth target"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
    SA$XY/sqrt(SA$XX*SA$YY))

vinfo(cat, "hard conditional... ")
timer <- proc.time()
storage.mode(Yh) <- "logical"
SA <- kQDM_SA(X[Yh,], Y[Yh, , drop = FALSE], norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "nkQDM Gauss hard cond"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
    SA$XY/sqrt(SA$XX*SA$YY))

vinfo(cat, "smooth conditional... ")
timer <- proc.time()
SA <- kQDM_SA(X, Y, w = ws, cond = "conditional", norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "nkQDM Gauss smooth cond"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
    SA$XY/sqrt(SA$XX*SA$YY))

vinfo(cat, "hard hybrid... ")
timer <- proc.time()
SA <- kQDM_SA(X, Y, w = wh, cond = "hybrid", norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "nkQDM Gauss hard hybrid"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
    SA$XY/sqrt(SA$XX*SA$YY))

vinfo(cat, "smooth hybrid... ")
timer <- proc.time()
SA <- kQDM_SA(X, Y, w = ws, cond = "hybrid", norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "nkQDM Gauss smooth hybrid"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
    SA$XY/sqrt(SA$XX*SA$YY))

###  Csiszar divergence dependence measure  ###
# set different functions defining Csiszar divergence
flist <- list(MI = fKL, TV = fTV)
Xcopu <- dfapply(X, copula_col)
Ycopu <- copula_col(Y)

vinfo(cat, "\ncompute normalized support Csiszar divergence dependence",
    "measure, k-nearest-neighbors density estimation:\n")
vinfo(cat, "global... ")
timer <- proc.time()
SA <- CDM_SA(Xcopu, Ycopu, f = flist, dest_kernel = trunc_knn_kernel_matrix,
    norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
for (fun in names(flist)){
    name <- paste("nsCDM", fun, "trunc knn copu")
    allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
        SA[[fun]]$XY/SA[[fun]]$XX)
}

vinfo(cat, "hard target (with empirical frequencies for Y)... ")
timer <- proc.time()
storage.mode(Yh) <- "logical"
SA <- CDM_SA(Xcopu, Yh, f = flist, dest_kernel = trunc_knn_kernel_matrix,
    norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
for (fun in names(flist)){
    name <- paste("nsCDM", fun, "knn hard target")
    allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
        SA[[fun]]$XY/SA[[fun]]$XX)
}

vinfo(cat, "smooth target... ")
timer <- proc.time()
SA <- CDM_SA(Xcopu, copula_col(Ys), f = flist, dest_kernel = 
    trunc_knn_kernel_matrix, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
for (fun in names(flist)){
    name <- paste("nsCDM", fun, "knn smooth target")
    allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
        SA[[fun]]$XY/SA[[fun]]$XX)
}

vinfo(cat, "hard conditional... ")
timer <- proc.time()
storage.mode(Yh) <- "logical"
SA <- CDM_SA(dfapply(X[Yh,], copula_col), copula_col(Y[Yh, , drop = FALSE]),
    f = flist, dest_kernel = trunc_knn_kernel_matrix, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
for (fun in names(flist)){
    name <- paste("nsCDM", fun, "knn hard cond")
    allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
        SA[[fun]]$XY/SA[[fun]]$XX)
}

vinfo(cat, "smooth conditional... ")
timer <- proc.time()
SA <- CDM_SA(dfapply(X, copula_col, w = ws), copula_col(Y, w = ws), f = flist,
    dest_kernel = trunc_knn_kernel_matrix, w = ws, cond = "conditional",
    norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
for (fun in names(flist)){
    name <- paste("nsCDM", fun, "knn smooth cond")
    allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
        SA[[fun]]$XY/SA[[fun]]$XX)
}

vinfo(cat, "hard hybrid... ")
timer <- proc.time()
SA <- CDM_SA(Xcopu, Ycopu, f = flist, dest_kernel = trunc_knn_kernel_matrix,
    w = wh, cond = "hybrid", norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
for (fun in names(flist)){
    name <- paste("nsCDM", fun, "knn hard hybrid")
    allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
        SA[[fun]]$XY/SA[[fun]]$XX)
}

vinfo(cat, "smooth hybrid... ")
timer <- proc.time()
SA <- CDM_SA(Xcopu, Ycopu, f = flist, dest_kernel = trunc_knn_kernel_matrix,
    w = ws, cond = "hybrid", norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
for (fun in names(flist)){
    name <- paste("nsCDM", fun, "knn smooth hybrid")
    allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
        SA[[fun]]$XY/SA[[fun]]$XX)
}

remove(Xcopu, Ycopu)

###  randomized maximum correlation  ###
vinfo(cat, "\ncompute randomized maximum correlation, copula transform and",
    "sine nonlinearities:\n")
vinfo(cat, "global... ")
timer <- proc.time()
SA <- RMC_SA(X, Y)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "RMC2 copu sine"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA)

vinfo(cat, "hard conditional... ")
timer <- proc.time()
storage.mode(Yh) <- "logical"
SA <- RMC_SA(X[Yh,], Y[Yh, , drop = FALSE])
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "RMC2 copu sine hard cond"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA)

vinfo(cat, "smooth conditional... ")
timer <- proc.time()
SA <- RMC_SA(X, Y, w = ws)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "RMC2 copu sine smooth cond"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA)

vinfo(cat, "\nestimate correlation ratio with randomized maximum",
    "correlation, copula transform and sine nonlinearities:\n")
vinfo(cat, "global... ")
timer <- proc.time()
SA <- RMC_SA(X, Y, fY = identity)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "RMC corr2 copu sine"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA)

vinfo(cat, "hard target... ")
timer <- proc.time()
storage.mode(Yh) <- "double"
SA <- RMC_SA(X, Yh, fY = identity)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "RMC corr2 sine hard target"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA)

vinfo(cat, "smooth target... ")
timer <- proc.time()
SA <- RMC_SA(X, Ys, fY = identity)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "RMC corr2 sine smooth target"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA)

vinfo(cat, "hard conditional... ")
timer <- proc.time()
storage.mode(Yh) <- "logical"
SA <- RMC_SA(X[Yh,], Y[Yh, , drop = FALSE], fY = identity)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "RMC2 corr2 sine hard cond"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA)

vinfo(cat, "smooth conditional... ")
timer <- proc.time()
SA <- RMC_SA(X, Y, w = ws, fY = identity)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "RMC2 corr2 sine smooth cond"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA)

vinfo(cat, "\nestimate correlation ratio with randomized maximum",
    "correlation, copula transform and logistic nonlinearities:\n")
vinfo(cat, "hard target... ")
timer <- proc.time()
storage.mode(Yh) <- "double"
SA <- RMC_SA(X, Yh, fX = transf_norm_fun(copula_col_one, k = round(sqrt(n)),
    f = logistic), fY = identity)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "RMC corr2 logi hard target"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA)


vinfo(cat, "smooth target... ")
timer <- proc.time()
SA <- RMC_SA(X, Ys, fX = transf_norm_fun(copula_col_one, k = round(sqrt(n)),
    f = logistic), fY = identity)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "RMC corr2 logi smooth target"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA)

} # endfor r in 1L:nrep

                         #--------------------------#
                         #  print and save results  #
                         #--------------------------#
.verbose = verbose
vinfo(cat, "\n\t\t###  average sensitivity measures over", nrep,
    model$sampling, "sample(s) of size", n, " ###\n")
# if there is too many input factors, try the transpose
vinfo(print, do.call(rbind, lapply(allSA[[nn]], colMeans)))

if (model$sampling == "bootstrap"){
    vinfo(cat, "\n\t\t##  bootstrap standard deviation  ##\n")
    vinfo(print, do.call(rbind, lapply(allSA[[nn]], std_col)))
}else{ # permutation
    vinfo(cat, "\n\t\t##  values on actual observations  ##\n")
    vinfo(print, do.call(rbind, lapply(allSA[[nn]], function(SA){ SA[nrep,] })))
}

} #enfor n in samples

expname = num2str(list(model$name, model$sampling, cr = model$cr, ws = 
    weight_smoothness), len = 1)
expname <- paste("PBSA_", gsub("[ :]+", "_", expname), sep ="")
expdir <- file.path(resdir, expname)

vinfo(cat, "saving results in ", expdir, "... ", sep = "")
if (!dir.exists(resdir)){ dir.create(resdir) }
if (!dir.exists(expdir)){ dir.create(expdir) }

save(model, allSA, cr, weight_smoothness, file = file.path(expdir, 
    "model_SA.rdata"))

pdf(file.path(expdir, "boxplots_summary.pdf"), 
    width = length(allSA)*max(ncol(X)/3 + 1, 2.8), height = 2)
#        layout            vert X label          bot left top right
par(mfcol = c(1, length(allSA)), las = 2, mai = c(.4, .6, .5, .5))
for (method in names(allSA[[1]])){
    for (nn in names(allSA)){
        nrep <- repetitions[[nn]]
        boxplot(allSA[[nn]][[method]])
        x <- 1L:model$Xparam$d
        y <- allSA[[nn]][[method]][nrep,] # last one uses actual observations
        points(x = x, y = y, pch = 1, cex = 1, col = "red")
        if (model$sampling == "permutation"){
            rk <- 1 + colSums(allSA[[nn]][[method]] > rep(y, each = nrep)) + 
                  (colSums(allSA[[nn]][[method]] == rep(y, each = nrep)) - 1)/2
            text(x = x, y = max(allSA[[nn]][[method]]), adj = c(-.5, 1), 
                labels = paste(rk), cex = 1, col = "red")
        }
        title(paste(method, "\n", nn, "rep =", repetitions[[nn]]))
    }
}
dev.off()
vinfo(cat, "done.\n")

# copy and paste the following for commenting out big sections of code
if (FALSE){ ######## CODE BELOW THIS LINE IS NOT EXECUTED #####################
} ################## CODE ABOVE THIS LINE IS NOT EXECUTED #####################
