    #--------------------------------------------------------------------#
    #  script for testing total versions of global sensitivity analysis  #
    #--------------------------------------------------------------------#
if (FALSE){ ######## CODE BELOW THIS LINE IS NOT EXECUTED #####################
# Hugo Raguet 2017
# check availability of needed packages and source files below
source("Sensitivity_analysis/sensitivity_measures.r", chdir = TRUE)
source("Sensitivity_analysis/Tests/test_models.r")

                           #----------------------#
                           #  general parameters  #
                           #----------------------#
samples <- c(50L, 200L, 500L, 1000L) # sample sizes to test
repetitions <- 100L # number of independent repetitions for statistics
                    # can be different for each sample size

verbose = TRUE # print info on process and results
.verbose = verbose # variable for function `vinfo`
# number of significant digits and scientific notation when printing
# intermediate results on terminal
options(digits = 3, scipen = -3)


# directory to print results
resdir <- "Sensitivity_analysis/Results"

                            #--------------------#
                            #  model definition  #
                            #--------------------#
##  select model below or preload your own; see test_models.r
Sobol_g_model(a = c(0, 1, 9, 99), sampling = "random")
# Ishigami_Homma_model(a = 5, b = .1, sampling = "space filling")
} ################## CODE ABOVE THIS LINE IS NOT EXECUTED #####################

vinfo(cat, "\n\t\t###  Test with", model$name, "model  ###\n")
vinfo(cat, "parameters for input factors:", num2str(model$Xparam), "\n")
vinfo(cat, "parameters for model function:", num2str(model$fparam), "\n")
vinfo(cat, "model sampling scheme:", model$sampling, "\n")

                               #--------------#
                               #  processing  #
                               #--------------#
# initialize data frame storing results
repetitions <- rep_len(repetitions, length.out = length(samples))
names(repetitions) <- lapply(samples, function(n){ paste("n = ", n, sep = "")
    })
allSA <- lapply(repetitions, function(nrep){ data.frame(matrix(nrow = nrep,
    ncol = 0)) })

for (n in samples){
    nn <- paste("n =", n)
    nrep <- repetitions[[nn]]
    vinfo(cat, "\n\t\t###  compute sensitivity measures for", nrep,
        "different sample(s) of size", n, " ###\n")
    progress_message <- ""

for (r in 1L:nrep){
    .verbose = verbose
    vinfo(cat, rep.int("\b", nchar(progress_message)), sep = "")
    progress_message = paste("\nrepetition #", r, "/", nrep, sep ="")
    vinfo(cat, progress_message)
    if (r == 1){ progress_message = "" }
    else{ .verbose = FALSE }

    vinfo(cat, "\nsampling ", model$sampling, "... ", sep = "")
    timer <- proc.time()
    XY <- model$sample(n)
    X <- XY$X; Y <- XY$Y; remove(XY)
    # create data frame for "total indices"
    XX <- data.frame(matrix(nrow = n, ncol = model$Xparam$d))
    names(XX) <- names(X)
    for (i in 1L:model$Xparam$d){ XX[i] <- data.matrix(X[,-i]) }
    timer <- proc.time() - timer
    vinfo(cat, "done; elapsed time:", timer[3], "s\n")

###  Sobol indices  ###
vinfo(cat, "\ncompute pick-and-freeze correlation ratio indices, first- and",
    "total-order, Saltelli plan, Monod method... ")
timer <- proc.time()
SA <- pick_freeze_corr_ratio_SA(Saltelli_plan(X[,1L:model$Xparam$d], model$f))
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "PF corr2 1"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA$S1)
name <- "PF corr2 T"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA$ST)

###  kernel quadratic dependence measure  ###
vinfo(cat, "\ncompute normalized kernel quadratic dependence measure,",
    "Gaussian kernel, bandwidth determined by empirical standard deviation of",
    "each coordinate:\n")
vinfo(cat, "first-order... ")
timer <- proc.time()
SA <- kQDM_SA(X, Y, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "nkQDM Gauss std coor 1"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
    SA$XY/sqrt(SA$XX*SA$YY))

vinfo(cat, "total-order... ")
timer <- proc.time()
SA <- kQDM_SA(XX, Y, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "nkQDM Gauss std coor T"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
    1 - SA$XY/sqrt(SA$XX*SA$YY))

vinfo(cat, "\ncompute normalized kernel quadratic dependence measure,",
    "Gaussian kernel, bandwidth determined by median of the observed",
    "distances:\n")
vinfo(cat, "first-order... ")
timer <- proc.time()
SA <- kQDM_SA(X, Y, kX = function(X){ rbf_kernel_matrix(X, SX = 1, SD = median)
     }, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "nkQDM Gauss med dist 1"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
    SA$XY/sqrt(SA$XX*SA$YY))

vinfo(cat, "total-order... ")
timer <- proc.time()
SA <- kQDM_SA(XX, Y, kX = function(X){ rbf_kernel_matrix(X, SX = 1, SD = 
    median) }, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "nkQDM Gauss med dist T"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
    1 - SA$XY/sqrt(SA$XX*SA$YY))

Xcopu <- dfapply(X, copula_col)
XXcopu <- dfapply(XX, copula_col)
Ycopu <- copula_col(Y)

vinfo(cat, "\ncompute normalized kernel quadratic dependence measure,",
    "Gaussian kernel over copula transforms, bandwidth determined by",
    "empirical standard deviation of each coordinate:\n")
vinfo(cat, "first-order... ")
timer <- proc.time()
SA <- kQDM_SA(Xcopu, Ycopu, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "nkQDM copu Gauss std coor 1"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
    SA$XY/sqrt(SA$XX*SA$YY))

vinfo(cat, "total-order... ")
timer <- proc.time()
SA <- kQDM_SA(XXcopu, Ycopu, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "nkQDM copu Gauss std coor T"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
    1 - SA$XY/sqrt(SA$XX*SA$YY))

vinfo(cat, "\ncompute normalized kernel quadratic dependence measure,",
    "Gaussian kernel over copula transforms, bandwidth determined by",
    "median of the observed distances:\n")
vinfo(cat, "first-order... ")
timer <- proc.time()
SA <- kQDM_SA(Xcopu, Ycopu, kX = function(X){
    rbf_kernel_matrix(X, SX = 1, SD = median) }, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "nkQDM copu Gauss med dist 1"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
    SA$XY/sqrt(SA$XX*SA$YY))

vinfo(cat, "total-order... ")
timer <- proc.time()
SA <- kQDM_SA(XXcopu, Ycopu, kX = function(X){
    rbf_kernel_matrix(X, SX = 1, SD = median) }, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "nkQDM copu Gauss med dist T"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
    1 - SA$XY/sqrt(SA$XX*SA$YY))

###  Csiszar divergence dependence measure  ###
# set different functions defining Csiszar divergence
flist <- list(MI = fKL, TV = fTV)

vinfo(cat, "\ncompute normalized support Csiszar divergence dependence",
    "measure, Gaussian kernel density estimation:\n")
vinfo(cat, "first-order... ")
timer <- proc.time()
SA <- CDM_SA(X, Y, f = flist, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
for (fun in names(flist)){
    name <- paste("nsCDM", fun, "Gauss Silverman 1")
    allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
        SA[[fun]]$XY/SA[[fun]]$XX)
}

vinfo(cat, "total-order... ")
timer <- proc.time()
SA <- CDM_SA(XX, Y, f = flist, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
for (fun in names(flist)){
    name <- paste("nsCDM", fun, "Gauss Silverman T")
    allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
        1 - SA[[fun]]$XY/SA[[fun]]$XX)
}

vinfo(cat, "\ncompute normalized support Csiszar divergence dependence",
    "measure, k-nearest-neighbors density estimation:\n")
vinfo(cat, "first-order... ")
timer <- proc.time()
SA <- CDM_SA(Xcopu, Ycopu, f = flist, dest_kernel = 
    trunc_knn_kernel_matrix, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
for (fun in names(flist)){
    name <- paste("nsCDM", fun, "trunc knn copu 1")
    allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
        SA[[fun]]$XY/SA[[fun]]$XX)
}

vinfo(cat, "total-order... ")
timer <- proc.time()
SA <- CDM_SA(XXcopu, Ycopu, f = flist, dest_kernel = 
    trunc_knn_kernel_matrix, norm = TRUE)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
for (fun in names(flist)){
    name <- paste("nsCDM", fun, "trunc knn copu T")
    allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 
        1 - SA[[fun]]$XY/SA[[fun]]$XX)
}

remove(Xcopu, XXcopu, Ycopu)

###  randomized maximum correlation  ###
vinfo(cat, "\ncompute randomized maximum correlation, copula transform and",
    "sine nonlinearities:\n")
vinfo(cat, "first-order... ")
timer <- proc.time()
SA <- RMC_SA(X, Y)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "RMC2 copu sine 1"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA)

vinfo(cat, "total-order... ")
timer <- proc.time()
SA <- RMC_SA(XX, Y)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "RMC2 copu sine T"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 1 - SA)

vinfo(cat, "\nestimate correlation ratio with randomized maximum",
    "correlation, copula transform and sine nonlinearities:\n")
vinfo(cat, "first-order... ")
timer <- proc.time()
SA <- RMC_SA(X, Y, fY = identity)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "RMC corr2 copu sine 1"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, SA)

vinfo(cat, "total-order... ")
timer <- proc.time()
SA <- RMC_SA(XX, Y, fY = identity)
timer <- proc.time() - timer
vinfo(cat, "done; elapsed time:", timer[3], "s\n")
name <- "RMC corr2 copu sine T"
allSA[[nn]][[name]] <- matrix_fill_row(allSA[[nn]][[name]], nrep, r, 1 - SA)

} # endfor r in 1L:nrep

                         #--------------------------#
                         #  print and save results  #
                         #--------------------------#
.verbose = verbose
vinfo(cat, "\n\t\t###  average sensitivity measures over", nrep, 
    "different sample(s) of size", n, " ###\n")
# if there is too many input factors, try the transpose
vinfo(print, do.call(rbind, lapply(allSA[[nn]], colMeans)))

} #enfor n in samples

if (model$sampling %in% c("observations", "bootstrap", "permutation")){
    expname = num2str(list(model$name, model$sampling), len = 1)
}else{
    expname = num2str(list(model$name, model$Xparam, model$fparam, 
        model$sampling), len = 1)
}
expname <- paste("GTSA_", gsub("[ :]+", "_", expname), sep ="")
expdir <- file.path(resdir, expname)

vinfo(cat, "saving results in ", expdir, "... ", sep = "")
if (!dir.exists(resdir)){ dir.create(resdir) }
if (!dir.exists(expdir)){ dir.create(expdir) }

save(model, allSA, file = file.path(expdir, "model_SA.rdata"))

pdf(file.path(expdir, "boxplots_summary.pdf"), 
    width = length(allSA)*max(ncol(allSA[[1]][[1]])/3 + 1, 2.5), height = 2)
#        layout            vert X label       bot left top  right
par(mfcol = c(1, length(allSA)), las = 2, mai = c(.4, 0.4, 0.5, 0.2))
for (method in names(allSA[[1]])){
    for (nn in names(allSA)){
        boxplot(allSA[[nn]][[method]])
        title(paste(method, "\n", nn, "rep =", repetitions[[nn]]))
    }
}
dev.off()
vinfo(cat, "done.\n")

# copy and paste the following for commenting out big sections of code
if (FALSE){ ######## CODE BELOW THIS LINE IS NOT EXECUTED #####################
} ################## CODE ABOVE THIS LINE IS NOT EXECUTED #####################
