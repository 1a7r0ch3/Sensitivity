      #----------------------------------------------------------------#
      #  simple model functions for testing senstivity analysis tools  #
      #----------------------------------------------------------------#
# A model is a list with the following named elements
#   name   - character string, the name of the model
#   Xparam - parameters of the number and distribution of the input factors
#   fparam - parameters of the function of the model
#   f      - function mapping (data frame) input factors to (matrix) output
#   sample - a sampling function mapping a positive integer to a list
#            containing (data frame) input factors and corresponding (matrix)
#            outputs of the desired sample size
#   sampling - (optional) information on the sampling scheme
# The functions defined in this file create such a model in the global 
# environment, instead of assigning a return value, because f and sample
# depends on fparam and Xsample, respectively. This might be more properly
# implemented as S4 class.
model <- list()

###  functions for random sampling or space filling  ###
DiceDesignAvailable <- "DiceDesign" %in% installed.packages()[,"Package"]
if (DiceDesignAvailable){
    library("DiceDesign")
    cat("Package DiceDesign available, use `sampling = 'space filling'` for",
        "sampling input factors with space filling design.\n")
    hypercube_filling <- function(n, d)
    #
    #   hypercube_filling(n, d) 
    # 
    # space filling design, n samples in hypercube [0,1]^d
    {
        it <- max(1e1, min(1e4, round(1e6/n)))
        return(discrepSA_LHS(lhsDesign(n, d)$design, it = it)$design)
    }
}else{
    cat("Package DiceDesign not available, install it for sampling input",
        "factors with space filling design.\n")
}

hypercube_sample <- function(n, d, sampling = "random")
#
#   hypercube_sample(n, d, sampling = "random") 
# 
# n uniform random samples in hypercube [0,1]^d, accoding to sampling scheme
{ 
    if (sampling == "space filling"){ return(hypercube_filling(n, d)) }
    else{ return(matrix(runif(n*d), nrow = n)) }
}

set_model_sampling <- function(sampling = "random")
#
#       set_model_sampling(sampling = "random")
# 
# set the model sampling scheme, check DiceDesign package if necessary
{
    if (sampling == "space filling" && !DiceDesignAvailable){
        warning("Package DiceDesign necessary for space filling design;",
            "fall back to random sampling.\n")
        sampling <- "random"
    }
    model$sampling <<- sampling
}

###  example of model definition  ###
Sobol_g_model <- function(a = NULL, d = length(a), sampling = "random")
#
#       Sobol_g_model(a = NULL, d = length(a))
#
# model with function g of Sobol', used for instance by Saltelli and Sobol'
# (1995), 
#
#   x -> prod_{i=1}^d (|4 xi - 2| + ai)/(1 + ai) ,
#
# where xi ~ U(0, 1) i.i.d., and the ai are positive coefficients;
# the highest the ai, the less important the xi
# default values used by Crestaux et al. (2009), ai = (i - 1)/2
#
# Hugo Raguet 2017
{
    if (is.null(a)){ 
        if (d == 0){ stop("provide either argument `a` or argument `d`") }
        a <- (1:d - 1)/2
    }
    model$name <<- "Sobol' g"
    model$Xparam <<- list(d = d)
    model$fparam <<- list(a = a)
    model$f <<- function(X){
        Y <- apply(X, 1, function(x){ prod((abs(4*x - 2) + 
            model$fparam$a)/(1 + model$fparam$a)) } )
        dim(Y) <- c(length(Y), 1)
        return(Y)
    }
    set_model_sampling(sampling)
    model$sample <<- function(n){
        X <- hypercube_sample(n, model$Xparam$d, model$sampling)
        X <- data.frame.mat(X)
        return(list(X = X, Y = model$f(X)))
    }
}

Ishigami_Homma_model <- function(a = 7, b = .1, sampling = "random")
#
#       Ishigami_Homma_model(a = 7, b = .1, sampling = "random")
#
# model introduced by Ishigami and Homma (1990), with function
#
#   (x1, x2, x3) -> sin(x1) + a sin(x2)^2 + b (x3)^4 sin(x1) ,
#
# where xi ~ U(-pi, pi) i.i.d. and a and b are positive coefficients
# 
# Hugo Raguet 2017
{
    model$name <<- "Ishigami and Homma"
    model$Xparam <<- list(d = 3)
    model$fparam <<- list(a = a, b = b)
    model$f <<- function(X){ sin(X[[1]])*(1 + model$fparam$b*X[[3]]^4) +
        model$fparam$a*sin(X[[2]])^2 }
    set_model_sampling(sampling)
    model$sample <<- function(n){
        X <- hypercube_sample(n, model$Xparam$d, model$sampling)
        X <- -pi + 2*pi*X
        X <- data.frame.mat(X)
        return(list(X = X, Y = model$f(X)))
    }
}

affine_critical_model <- function(xc = .9, cr = 1, smax = 10, sampling = "random")
#
#       affine_critical_model(xc = .9, cr = 1, smax = 10, sampling = "random")
#
# model for testing target sensibility analysis with function
#
#       (s, x) -> s*(x - xc) + cr ,
#
# where x ~ U(0, 1), and s ~ U(0, smax), typically smax >> 1
# the slope s can induce much variability, but only x determines if the output
# exceeds the critical value
# 
# INPUTS:
#   xc   - the value of x above which the output exceeds the critical value
#   cr   - the "critical" output value
#   smax - maximum value of slope s, typically smax >> 1
#
# Hugo Raguet 2017
{
    model$name <<- "affine critical" 
    model$Xparam <<- list(d = 2, smax = smax)
    model$fparam <<- list(xc = xc, cr = cr)
    model$f <<- function(X){ X$S*(X$X - model$fparam$xc) + model$fparam$cr }
    set_model_sampling(sampling)
    model$sample <<- function(n){
        X <- hypercube_sample(n, model$Xparam$d, model$sampling)
        X <- data.frame.mat(S = model$Xparam$smax*X[,1], X = X[,2])
        return(list(X = X, Y = model$f(X)))
    }
}

min_norm_unif <- function(s = 1, sampling = "random")
#
#       min_norm_unif(s = 1, sampling = "random")
#
# model with minimum function
#
#   (N, U) -> min(N, U) ,
#
# where U ~ U(0, 1) and N ~ N(0, s) is normally distributed with variance s,
#
# Hugo Raguet 2017
{
    model$name <<- "minimum normal uniform"
    model$Xparam <<- list(d = 2, s = s)
    model$fparam <<- NULL
    model$f <<- function(X){ pmin(X$N, X$U) }
    set_model_sampling(sampling)
    model$sample <<- function(n){
        X <- hypercube_sample(n, model$Xparam$d, model$sampling)
        X <- data.frame.mat(N = model$Xparam$s*qnorm(X[,1]), U = X[,2])
        return(list(X = X, Y = model$f(X)))
    }
}

awgn_indpdt_model <- function(s = .1, sampling = "random")
#
#       awgn_indpdt_model(s = .1, sampling = "random")
#
# model with small additive white Gaussian noise, and an independent variable
#
#   (X, N, I) -> X + N + I,
#
# where X ~ N(0, 1), N ~ N(0, s) and I ~ N(0, 1) are normally distributed with
# variances unity, s, and unity, respectively.
#
# Hugo Raguet 2017
{
    model$name <<- "small additive noise and independent variable"
    model$Xparam <<- list(d = 3, s = s)
    model$fparam <<- NULL
    model$f <<- function(X){ X[[1]] + X[[2]] }
    set_model_sampling(sampling)
    model$sample <<- function(n){
        X <- hypercube_sample(n, model$Xparam$d, model$sampling)
        X <- data.frame.mat(X = qnorm(X[1,]), N = model$Xparam$s*qnorm(X[2,]), 
                I = qnorm(X[3,]))
        return(list(X = X, Y = model$f(X)))
    }
}

model_from_observations <- function(X, Y, name = "custom", sampling = "observations")
#
#       model_from_observations(X, Y, name = custom, sampling = "observations")
#
# constructing model from observations
# 
# INPUTS
#   X        - n-by-d data frame, n observations of d input factors, each as
#              n-by-pi matrix, in dimension pi.
#   Y        - n-by-q matrix, n model outputs of dimension q
#   name     - character string specifying the name of the model
#   sampling - information on sampling scheme; set to "observations" for
#              returning the whole observed data, and to "bootstrap" for
#              resampling with replacement within the observed data, and to
#              "permutation" for permuting randomly the outputs
#
# Hugo Raguet 2017
{
    model$name <<- name
    model$Xparam <<- list(d = length(X), X = X)
    model$fparam <<- list(Y = Y)
    model$f <<- NULL
    set_model_sampling(sampling)
    model$sample <<- function(n = nrow(model$Xparam$X)){
        if (model$sampling == "bootstrap"){
            idx <- sample(n, replace = TRUE)
            return(list(X = model$Xparam$X[idx,], Y = model$fparam$Y[idx, ,
                drop = FALSE]))
        }else if (model$sampling == "permutation"){
            idx <- sample(n, replace = FALSE)
            return(list(X = model$Xparam$X[1L:n,], Y = model$fparam$Y[idx, , 
                drop = FALSE]))
        }else{
            return(list(X = model$Xparam$X, Y = model$fparam$Y))
        }
    }
}
