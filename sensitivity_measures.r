###  must be sourced with option chdir = TRUE  ###
              #------------------------------------------------#
              #  functions and tools for sensitivity measures  #
              #------------------------------------------------#
source("Sensitivity_measures/tools.r", chdir = TRUE)
source("Sensitivity_measures/pick_freeze_corr_ratio.r")
source("Sensitivity_measures/kernel_quadratic_dependence_measure.r")
source("Sensitivity_measures/Csiszar_div_dependence_measure.r")
source("Sensitivity_measures/randomized_maximum_correlation.r")

                            #---------------------#
                            #  data manipulation  #
                            #---------------------#
# Many functions assumes that data are stored in matrices with one observation
# by rows. This includes unidimensional observations, which must be stored as
# column matrices and not as vectors. To populate data frames with matrices
# instead of vectors, use the functions data.frame.mat and read.table.mat
data.frame.mat <- function(...)
#
#       data.frame.mat(...)
#
# calls data.frame and set `dim` attribute to vector data
#
# Hugo Raguet 2017
{
    DF <- data.frame(...)
    for (i in 1L:length(DF)){
        if (is.null(dim(DF[[i]]))){ dim(DF[[i]]) <- c(length(DF[[i]]), 1) }
    }
    return(DF)
}

read.table.mat <- function(...)
#
#       read.table.mat(...)
#
# calls data.frame and set `dim` attribute to vector data
#
# Hugo Raguet 2017
{
    DF <- read.table(...)
    for (i in 1L:length(DF)){
        if (is.null(dim(DF[[i]]))){ dim(DF[[i]]) <- c(length(DF[[i]]), 1) }
    }
    return(DF)
}

dfapply <- function(DF, f, ...)
#
#       dfapply <- function(DF, f, ...)
#   
# apply function f to each element of data frame DF
#
# Hugo Raguet 2017
{
    for (i in 1L:length(DF)){ DF[[i]] <- f(DF[[i]], ...) }
    return(DF)
}

# the following is useful for populating data frames whose columns are not
# known in advance
matrix_fill_row <- function(M, n, i, dat)
#
#       matrix_fill_row(M, n, i, dat)
#
# fill i-th row of given matrix with data `dat`; if M is null, create it first
# filled with NA, with number of column and column names taken from `dat`
#
# INPUTS:
#   M   - NULL or n-by-p matrix
#   n   - number of rows of the matrix
#   i   - index of row to be filled
#   dat - vector of length d
#   
# OUTPUT:
#   n-by-d matrix
#
# Hugo Raguet 2017
{
    d = length(dat)
    if (is.null(M)){
        M <- matrix(NA, nrow = n, ncol = d)
        colnames(M) <- names(dat)
    }
    M[i,] <- dat
    return(M)
}

                        #-----------------------------#
                        #  string and info formating  #
                        #-----------------------------#
# print information only if variable `.verbose` exists and evaluates to TRUE
vinfo <- function(fun, ...){ if (exists(".verbose") && .verbose){ fun(...) } }

num2str <- function(num, len = 3, typ = "scientific", sep = "  ", namesep = 
    ": ", maxelem = 6L)
#
#   num2str(num, len = 3, typ = "scientific", sep = " ", namesep = ": ",
#	    maxelem = 6L)
#
# concatenate numbers into a string of characters, with desired notation and
# significant digits. Integers are kept as such if their decimal representation
# has smaller length.
# if objects are named, the name is also included.
#
# INPUTS:
#   num - list or vector of numbers or strings; or list of such lists or
#         vectors, the function is then applied recursively
#   len - tunes the number of digits; for "scientific" notation, it is the
#         number of significant digits; for "decimal" notation, it is the
#         number of digits behind the decimal separator;
#         several can be specified, the last one being repeated if necessary
#   typ - type of representation; one of ["scientific"] | "decimal" |
#         "nodecimal"  | "shortest";  "nodecimal" uses a power notation with
#         an integer mantissa (avoiding thus a decimal separator); "shortest"
#         gives the shortest between scientific and decimal notations
#   sep - if several numbers are given, outputs are concatenated with the
#         separator `sep` in between
#   namesep - if objects are named, the name and the object are concatenated
#         with the separator `namesep` in between
#   maxelem - maximum number of elements to be taken into account (avoid
#         printing of big arrays)
#
# Hugo Raguet 2017
{
    if (is.null(num)){ return("") }
    s <- ""
    for (i in 1L:min(length(num), maxelem)){
        n <- num[[i]]
        if (!is.null(names(num)) && names(num)[[i]] != ""){
            s <- sprintf("%s%s%s", s, names(num)[[i]], namesep)
        }
        l <- len[min(i, length(len))]
        if (is.list(n) || length(n) != 1){
            s <- sprintf("%s%s%s", s, num2str(n, len = len, typ = typ, sep = 
                sep, namesep = namesep, maxelem = maxelem), sep)
        }else if (is.character(n) || !is.finite(n)){
            s <- sprintf("%s%s%s", s, paste(n), sep)
        }else if (round(n) == n && n < 10^(l+2)){
            s <- sprintf("%s%d%s", s, n, sep)
        }else{
            p <- floor(log10(abs(n)))
            if (typ == "nodecimal"){
                s <- sprintf("%s%de%d%s", s, round(n*10^(l - p - 1)),
                    p + 1 - l, sep)
            }else{
                if (typ == "scientific" || typ == "shortest"){
                    if (p==0){
                        sci <- sprintf(sprintf("%%s%%.%df", l - 1), s, n)
                    }else{
                        d <- n/10^p
                        sci <- sprintf(sprintf("%%s%%.%dfe%d", l - 1, p), s, d)
                    }
                }
                if (typ == "decimal" || typ == "shortest"){
                    # dec <- sprintf(sprintf("%%s%%.%df", l - p - 1), s, n)
                    dec <- sprintf(sprintf("%%s%%.%df", max(l, -p)), s, n)
                }
                if (typ == "scientific"){ s <- sci }
                else if (typ == "decimal"){ s <- dec }
                else if (typ == "shortest"){ 
                    if (l >= -p && nchar(dec) < nchar(sci)){ s <- dec }
                    else{ s <- sci }
                }
            }
            s <- paste(s, sep, sep = "")
        }
    }
    return(gsub(sprintf("%s$", sep), "", s))
}

num2expr <- function(num, len = 3, typ = "scientific")
#
#	     num2expr(num, len = 3, typ = "scientific")
#
# format numbers into character a string or an expression to use with plotmath,
# with desired notation and significant digits. Integers are kept as such if
# their decimal representation has smaller length.
# if objects are named, the name is also included.
#
# INPUTS:
#   num - vector of numbers
#   len - tunes the number of digits; for "scientific" notation, it is the
#         number of significant digits; for "decimal" notation, it is the
#         number of digits behind the decimal separator;
#         several can be specified, the last one being repeated if necessary
#   typ - type of representation; one of ["scientific"] | "decimal" |
#         "nodecimal"  | "shortest";  "nodecimal" uses a power notation with
#         an integer mantissa (avoiding thus a decimal separator); "shortest"
#         gives the shortest between scientific and decimal notations
#
# Hugo Raguet 2017
{
    exprsci <- expression(m %*% 10^e)
    expr <- sapply(num, function(n){
        n <- num2str(n, len = len, typ = typ)
        if (grepl("e", n)){
            me <- as.list(strsplit(n, "e")[[1]])
            names(me) <- c("m", "e")
            n <- do.call("substitute", list(exprsci[[1]], me))
        }
        return(n)
    })
    return(as.expression(expr))
}
