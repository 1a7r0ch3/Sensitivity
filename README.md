# Global, Target and Conditional Sensitivity Analysis with Dependence Measures
Routines in R with C/C++ support with Rcpp API.  
Parallel implementation with OpenMP API.  

## General Problem Statements
Given a phenomenon _Y_ determined by several factors _X_<sub>1</sub>,..., _X_<sub>d</sub>, the classical probabilistic framework is to suppose a deterministic relationship 

    _Y_ = _f_(_X_<sub>1</sub>,..., _X_<sub>d</sub>) ,  

and to model the factors as random variables to take into account uncertainties.  

The question is to determine _which factors are the most inflential_ without any knowledge of the function _f_, which can be arbitrarily complex, only with statistical tools applied on observations of factors and corresponding phenomenon. 
The typical framework is the case of complex numerical simulators of phenomenon whose properties are impossible to study analytically.  

## Global Sensitivity Analysis with Dependence Measures
Currently, the most popular sensitivity analysis tools are based on correlation ratios (often coined Sobol' indices).
Moreover, classical estimation procedures implies specific experience plans, with sample size growing with the number of factors to study, and assuming independence of the factors (so-called pick-and-freeze estimators).  

An interesting alternative is the sensitivity analysis based on dependence measures. 
We propose efficient and robust estimators of the following dependence measures:  
 * kernel quadratic dependence measure (also called Hilbert−Schmidt independence criterion)
 * Csiszar divergence dependence measures, such as mutual information, total variation distance, Neyman and Pearson χ²
 * randomized maximum correlation (based on a concept called randomized dependence coefficient)
  
They can be computed from any sample of observations. Such sample must be representative of the factors distributions, but analytical knowledge of these distributions is not required (hence direct physical measurements can be used as well), nor is assumption of factors independence.  

## Target and Conditional Sensitivity
It often happen that the investigator is interested only on the effect of the factors regarding a specific _critical_ region of values of the phenomenon, _C_ ⊂ ran(_Y_).  
This would be typically the case in studies of industrial safety or reliability, or insurance policies, where the critical domain can be for instance an excess in temperature or constraints, or in loss, respectively.  

We distinguish two different objectives:
 * target analysis, where the investigator is interested in the _occurence_ of the phenomenon in the critical region (which factors make the phenomenon critical)
 * conditional analysis, where the investigator is interested in the sensitivity of the phenomenon _within_ the critical domain (given the fact that the phenomenon is critical, which factors influence it most).

It turns out that dependence measures are convenient tools which can be tailored to address these questions. This is enabled in our implementation.

## Documentation
Articles describing in details the above tools are in preparation.  
The dependence measures are implemented in their eponymous R files within the directory `Sensitivity_measures/`.  
Some generic numerical routines are defined in `tools.r`, with speed-up routines written in C++ in `tools.cpp`; this requires the package `Rcpp` and we strongly recommend to set up the system with Open MP for enjoying parallelization.   
The script `sensitivity_measures.r` loads all these tools within the R environment (source it with option `chdir = TRUE`), together with convenient functions for manipulating data.  
Finally, the scripts in `Tests/` exemplify the use of the various sensitivity measures; in particular, `test_models.r` describes a convenient data structure for using our tools.  
